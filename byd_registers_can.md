## BYD CAN Registers

These registers resemble the ones in modbus a lot. Initially information regarding these registers was gathered from https://github.com/SunshadeCorp/can-service.


BYD/Emulator sending:

```
Send this after receiving a '1' in first octet in 0x151:
0x250, 'data':'\x03\x16\x00\x66\x00\xf0\x02\x09',   #3.16 = BYD firmware version, 00f0=240 <> 24kWh battery
0x290, 'data':'\x06\x37\x10\xd9\x00\x00\x00\x00',
0x2d0, 'data':'\x00' + 'BYD' + '\x00\x00\x00\x00',
0x3d0, 'data':'\x00' + 'Battery',
0x3d0, 'data':'\x01' + '-Box Pr',
0x3d0, 'data':'\x02' + 'emium H',
0x3d0, 'data':'\x03' + 'VS' + '\x00' * 5,

Every 2 seconds:
0x110: [2 bytes, max_voltage * 10 ], [2 bytes, min_voltage * 10 ], [2 bytes, discharge_max_current * 10 ], [2 bytes, charge_max_current * 10 ]

Every 10 seconds:
0x150: [2 bytes, soc * 100], [2 bytes, soh * 100], [2 bytes, target_discharge_current * 10], [2 bytes, target_charge_current * 10] 
0x1d0: [2 bytes, batt_voltage * 10], [2 bytes, batt_current * 10], [2 bytes, batt_temp * 10], [2 bytes (776), ???] 
0x210: [2 bytes, cell_temp_max * 10], [2 bytes, cell_temp_min * 10],0,0,0,0

Every 60 seconds:
0x190: 0,0,0,3,0,0,0,0          This is most likely status (as in modbus registers), but at least Sungrow ignores this
00 00 00 03 00 00 00 00


Note:
Sungrow reacts on current settings in 0x110 right away, use this for limiting current

```

Inverter sending (WIP):
```
At startup/scanning for batteries:

0x151 0053554E47524F57 [SUNGROW]
0x151 0100000000000000


Every 10 seconds:

(1706468868.184721) can1 091#0F38000100930100
(1706468868.234668) can1 0D1#03E8000000000200
(1706468868.284670) can1 111#65B6A5F600000000



0x091 0ED8000000940100 [0ed8 = 3800 (380,0V Measured HV?), 0094 = 148 <> 14.8C]
0x0D1 0258000000000200 [0258 = 600 <> 60% SoC]
0x111 65B3F94900000000 [65B3F949 = 1706293577 <> GMT: Friday 26 January 2024 18:26:17]

(1706469672.737955) can1 091#0F37000100950100
(1706469682.787836) can1 091#0F37000000950100
(1706469692.837850) can1 091#0F37000000950100
(1706469702.888467) can1 091#0F37000500950100
(1706469712.938146) can1 091#0F37000600950100
(1706469722.988154) can1 091#0F37000000950100
(1706469803.438315) can1 091#0F37000200950100
(1706469813.538544) can1 091#0F37000200950100
(1706469823.588566) can1 091#0F37000800950100

```
