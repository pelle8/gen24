#!/usr/bin/env python
'''
Parses a pcap with modbus data, extracts what registers that are read and written
2024 Per Carlen
'''

import argparse
import os
import sys
import binascii
import yaml
import json
import time

import paho.mqtt.client as paho
from datetime import datetime
from scapy.all import sniff
from scapy.utils import RawPcapReader
from scapy.layers.l2 import Ether
from scapy.layers.inet import IP, TCP

def decode_modbus(ip_pkt):
    global transaction_id,last_transaction_id
    global debug_raw

    if ip_pkt.proto != 6: 
        return #only tcp

    if ip_pkt.len < 41:
        return #Only packets with payload

    tcp_pkt = ip_pkt[TCP]
    if tcp_pkt.sport !=502 and  tcp_pkt.dport !=502 and tcp_pkt.sport !=1502 and  tcp_pkt.dport !=1502:
        return #Only modbus beyond this point

    payload_bytes = bytes(tcp_pkt.payload)
    if len(payload_bytes) < 7:
        return

    payload = binascii.hexlify(bytes(tcp_pkt.payload))
    if debug_raw: print("-----------------------")
    if debug_raw: print("TCP Payload:",payload)
    modbus = {}
    print_this_pkt = False
    exit_now = False
    transaction_id = payload_bytes[0]*256 + payload_bytes[1]
    if transaction_id == last_transaction_id:
        pkt_type = "response"
    else:
        pkt_type = "query"
    modbus['pkt_type'] = pkt_type
    last_transaction_id = payload_bytes[0]*256 + payload_bytes[1]
    modbus['transaction_id'] = payload_bytes[0]*256 + payload_bytes[1]
    modbus['protocol_id'] = payload_bytes[2]*256 + payload_bytes[3]
    modbus['length'] = payload_bytes[4]*256 + payload_bytes[5]
    modbus['unit_id'] = payload_bytes[6]
    modbus['function_code'] = payload_bytes[7]

    try: # Due to modbus protocol, we can't be sure it's a query or response...we may miss a packet and then we need to catch this error
        if pkt_type == "query":
            if modbus['function_code'] == 3: # Read holding registers
                modbus['reference_number'] = payload_bytes[8]*256 + payload_bytes[9]
                modbus['word_count'] = payload_bytes[10]*256 + payload_bytes[11]
            if modbus['function_code'] == 4: # Read input registers
                modbus['reference_number'] = payload_bytes[8]*256 + payload_bytes[9]
                modbus['word_count'] = payload_bytes[10]*256 + payload_bytes[11]
            if modbus['function_code'] == 6: # Write single register
                modbus['reference_number'] = payload_bytes[8]*256 + payload_bytes[9]
                modbus['word_count'] = 2
                modbus_data_bytes = payload_bytes[10:(10 + modbus['word_count'])]
                modbus_data = binascii.hexlify(bytes( modbus_data_bytes ))
                modbus['modbus_data'] = modbus_data
            if modbus['function_code'] == 16: # Write mutliple register
                modbus['reference_number'] = payload_bytes[8]*256 + payload_bytes[9]
                modbus['word_count'] = payload_bytes[10]*256 + payload_bytes[11]
                modbus_data_bytes = payload_bytes[13:(13 + 2 * modbus['word_count'])]
                modbus_data = binascii.hexlify(bytes( modbus_data_bytes ))
                modbus['modbus_data'] = modbus_data

        if pkt_type == "response":
            if modbus['function_code'] == 3: # Read holding registers
                modbus['byte_count'] = payload_bytes[8]
                modbus_data_bytes = payload_bytes[9:(9 + modbus['byte_count'])]
                if modbus['byte_count'] == len(modbus_data_bytes):
                    modbus_data = binascii.hexlify(bytes( modbus_data_bytes ))
                    modbus['modbus_data'] = modbus_data
                else:
                    print("Error in modbus packet",modbus['byte_count'],len(modbus_data_bytes))                   
            if modbus['function_code'] == 4: # Read input registers
                modbus['byte_count'] = payload_bytes[8]
                modbus_data_bytes = payload_bytes[9:(9 + modbus['byte_count'])]
                modbus_data = binascii.hexlify(bytes( modbus_data_bytes ))
                modbus['modbus_data'] = modbus_data
    except:
        pass

    modbus['exit_now'] = exit_now
    modbus['print_this_pkt'] = print_this_pkt
    return modbus


def decode_registers_raw(query_reference_number,modbus_data):
    registers = {}
    register_num = query_reference_number
    i = 0
    while i < len(modbus_data):
        value = int(modbus_data[i:i+4],16)
        registers[str(register_num)] = value
        register_num += 1
        i += 4
    return registers


def get_text_for_function_code(func_code):
    function_codes = { 3:'Read Holding', 4: 'Read Input', 6: "Write Single Holding", 16: "Write Multiple Holding" }
    if int(func_code) in function_codes:
        if int(func_code) == 6:
            if False: print("FC:",function_codes[int(func_code)])
            #exit(1)
        return function_codes[int(func_code)]
    return ""


def clean_string(s: str) -> str:
    return "".join(filter(lambda x: x.isalnum() or x.isspace(), s))

def decode_registers(function_code,registers_raw):
    global inverter_definitions

    registers_json = {}
    v = ""
    try:
        for idx, reg_data in enumerate(registers_raw):
            function_codes_text = get_text_for_function_code(function_code)
            reg_type = function_codes_text.split(" ")[-1].lower() # pick last word for type
            search_string = reg_data + "-" + reg_type
            if search_string in inverter_definitions:
                if inverter_definitions[search_string]['type'] == 'uint16':
                    v = registers_raw[reg_data]
                if inverter_definitions[search_string]['type'] == 'int16':
                    v = registers_raw[reg_data]
                    if v > 32768:
                        v = - (65535 - v)
                if inverter_definitions[search_string]['type'] == 'int32':
                    if reg_data in registers_raw and str(int(reg_data) + 1) in registers_raw:
                        v1 = registers_raw[reg_data]
                        v2 = registers_raw[str(int(reg_data) + 1)]
                        v = v1 + (v2 & 32767) * 65536
                        if v2 > 32768:
                            v = - ((65535 - v1) + (65535 - v2) * 65536)
                        idx += 1
                if 'string' in inverter_definitions[search_string]['type']:
                    v = ""
                    (tmp,word_count) = inverter_definitions[search_string]['type'].split('string')
                    for i in range (int(word_count)):
                        v0 = registers_raw[str(int(reg_data) + i)]
                        v2 = v0 % 256
                        v1 = v0 // 256
                        v += chr(v1) + chr(v2)
                        idx += 1    
                    v = clean_string(v)
                if 'multiplier' in inverter_definitions[search_string]:
                    v = v * float(inverter_definitions[search_string]['multiplier'])
                if 'value_decoder' in inverter_definitions[search_string]:
                    search_v = f"{v}"
                    if search_v in inverter_definitions[search_string]['value_decoder']:
                        v = inverter_definitions[search_string]['value_decoder'][search_v]
                if 'bit_decoder' in inverter_definitions[search_string]:
                    new_v = ""
                    for i in range(16):
                        bit_value = 2**i
                        bit_state = bit_value & int(v)
                        if bit_state > 0:
                            bit_state = 1
                        search_v = f"{i}"
                        if search_v in inverter_definitions[search_string]['bit_decoder']:
                            temp_vals = inverter_definitions[search_string]['bit_decoder'][search_v].split("|")
                            temp_v = temp_vals[bit_state]
                            new_v += temp_v + "|"
                    v = new_v
                registers_json[inverter_definitions[search_string]['description']] = v
    except:
        pass
    return registers_json


def decode_inverter_registers(modbus_packet,query_reference_number):
    global registers_json
    global mqtt_client1, mqtt_client_smarthome, mqtt_client_smarthome_client_id
    global registers_json
    global debug_raw
    global config
    global last_json_timestamp
    global last_publish

    registers_json['timestamp'] = str(int(time.time()))
    if 'modbus_data' in modbus_packet:
        if modbus_packet['function_code'] == 3 and modbus_packet['pkt_type'] == 'response':
            registers_raw = decode_registers_raw(query_reference_number,modbus_packet['modbus_data'])
            registers = decode_registers(modbus_packet['function_code'],registers_raw)
            if debug_raw: print("Decoded registers:",registers)
            registers_json = {**registers_json, **registers}
        if modbus_packet['function_code'] == 4 and modbus_packet['pkt_type'] == 'response':
            registers_raw = decode_registers_raw(query_reference_number,modbus_packet['modbus_data'])
            registers = decode_registers(modbus_packet['function_code'],registers_raw)
            if debug_raw: print("Decoded registers:",registers)
            registers_json = {**registers_json, **registers}
        if modbus_packet['function_code'] == 6 and modbus_packet['pkt_type'] == 'query':
            registers_raw = decode_registers_raw(query_reference_number,modbus_packet['modbus_data'])
            registers = decode_registers(modbus_packet['function_code'],registers_raw)
            if debug_raw: print("Decoded registers:",registers)
            registers_json = {**registers_json, **registers}
        if modbus_packet['function_code'] == 16 and modbus_packet['pkt_type'] == 'query':
            registers_raw = decode_registers_raw(query_reference_number,modbus_packet['modbus_data'])
            registers = decode_registers(modbus_packet['function_code'],registers_raw)
            if debug_raw: print("Decoded registers:",registers)
            registers_json = {**registers_json, **registers}

        # Sungrow doesn't represent the power of the battery with a sign, fix this...
        if config['decode_inverter'] == "sungrow":
            if 'Battery power' in registers_json and 'Running State' in registers_json and 'Battery forced charge discharge cmd' in registers_json:
                if registers_json['Battery forced charge discharge cmd'] == 204:
                    registers_json['Battery power'] = str(int(registers_json['Battery power']))
                else:    
                    if "|Discharging" in registers_json['Running State'] and int(registers_json['Battery power']) > 0:
                        registers_json['Battery power'] = str(-int(registers_json['Battery power']))
        
        json_string = json.dumps(registers_json, separators=(',', ':'))
        if debug_raw: print("-----------------------")

        if debug_raw: print("Registers(all):",json_string)
        print("Registers(all):",json_string)
        if 'mqtt_smarthome_broker' in config:
            try:
                if mqtt_client1.connected_flag:
                    rc = publish(mqtt_client1, json_string, "inverter/monitor")
            except:
                pass

            # Only publish once per second, at most...
            if last_json_timestamp != registers_json['timestamp']:
                last_json_timestamp = registers_json['timestamp']

                try:
                    if not mqtt_client_smarthome.connected_flag:
                        print("Smarthome MQTT not connected, trying to reconnect")
                        mqtt_client_smarthome.connect(config['mqtt_smarthome_broker'], config['mqtt_smarthome_port'], keepalive=60)
                        mqtt_client_smarthome.loop_start()
                    else:
                        rc = publish(mqtt_client_smarthome, json_string, "inverter/monitor/" + mqtt_client_smarthome_client_id)
                        print(last_json_timestamp,"Published?",rc)
                        if rc[0] != 0:
                            exit(1)
                        last_publish = int(time.time())
                except Exception as e: 
                    print(e)
                    exit(1)
                    
                print(last_json_timestamp,"Published, old:",last_publish)
                
                if int(last_publish) < int(last_json_timestamp) - 30:
                    exit(1)



def process_pcap(config):
    global transaction_id, last_transaction_id, query_reference_number
    global inverter_definitions
    global debug_raw

    if 'debug_raw' in config:
        debug_raw = config['debug_raw']
    else:
        debug_raw = 0

    file_name = config['pcap']
    if 'filter' in config:
        filter_register = config['filter']
    else:
        filter_register = "" 
    print('Opening {}...'.format(file_name), "with filter:",filter_register)
    last_transaction_id = -1
    collection = {}
    interesting_response_transaction = -1
    count = 0
    for (pkt_data, pkt_metadata,) in RawPcapReader(file_name):
        count += 1
        ether_pkt = Ether(pkt_data)
        if 'type' not in ether_pkt.fields:
           continue
        if ether_pkt.type != 0x0800:
           continue
        ip_pkt = ether_pkt[IP]

        modbus_packet = decode_modbus(ip_pkt)
        try:
            if modbus_packet['pkt_type'] == "query":
                pass
        except:
            continue
        if modbus_packet['pkt_type'] == "query":
            query_reference_number = modbus_packet['reference_number']
            if str(filter_register) == str(modbus_packet['reference_number']):
                modbus_packet['print_this_pkt'] = True
                interesting_response_transaction = modbus_packet['transaction_id']
            else:
                interesting_response_transaction = -1

            # Collect data, print summary later
            data_dict = {}
            if modbus_packet['function_code'] == 3: # Read holding registers
                data_dict = {'reference_number':modbus_packet['reference_number'],"word_count":modbus_packet['word_count']}
            if modbus_packet['function_code'] == 4: # Read input registers
                data_dict = {'reference_number':modbus_packet['reference_number'],"word_count":modbus_packet['word_count']}
            if modbus_packet['function_code'] == 6: # Write single register
                data_dict = {'reference_number':modbus_packet['reference_number'],"word_count":int(modbus_packet['word_count']/2)}
            if modbus_packet['function_code'] == 16: # Write multiple registers
                data_dict = {'reference_number':modbus_packet['reference_number'],"word_count":int(modbus_packet['word_count']/2)}
            if modbus_packet['function_code'] in collection:
                if not data_dict in collection[modbus_packet['function_code']]:
                    collection[modbus_packet['function_code']].append(data_dict)
            else:
                collection[modbus_packet['function_code']] = [data_dict]

        if modbus_packet['pkt_type'] == "response":
            if interesting_response_transaction ==  modbus_packet['transaction_id']:
#                if modbus_data_bytes[0] !=0x00 or modbus_data_bytes[1] !=0xf0:
#                    exit_now = True
                modbus_packet['print_this_pkt'] = True

        if filter_register == "":
            modbus_packet['print_this_pkt'] = True

        if modbus_packet['print_this_pkt']:
            timestamp = pkt_metadata.sec + pkt_metadata.usec/1000000
            dt_object = datetime.fromtimestamp(timestamp)
            print(f"{dt_object} - {modbus_packet}")
            if modbus_packet['exit_now']:
                print("Got exit condition")
                exit(1)
#        if ('modbus_data' in modbus_packet and modbus_packet['pkt_type'] == "response") or modbus_packet['pkt_type'] == "query":
        if len(inverter_definitions) > 0:
            ret = decode_inverter_registers(modbus_packet,query_reference_number)

    print("\nSummary:")
    for d in collection:
        print("Function code:",d)
        for i in collection[d]:
            print("Reference:",i['reference_number'],"word count:",i['word_count'])

def pkt_callback(pkt):
    global interesting_response_transaction, query_reference_number
    global last_packet_timestamp
    global debug_raw

    modbus_packet = decode_modbus(pkt[IP])

    if modbus_packet is None:
        return

    if modbus_packet['pkt_type'] == "query":
        modbus_packet['print_this_pkt'] = True
        if 'reference_number' in modbus_packet:
            query_reference_number = modbus_packet['reference_number']
        if 'transaction_id' in modbus_packet:
            interesting_response_transaction = modbus_packet['transaction_id']

    if modbus_packet['pkt_type'] == "response":
        if interesting_response_transaction ==  modbus_packet['transaction_id']:
            modbus_packet['print_this_pkt'] = True
        interesting_response_transaction = -1

    if 1: #modbus_packet['print_this_pkt']:
        last_packet_timestamp = time.time()
        dt_object = datetime.fromtimestamp(last_packet_timestamp)
        if debug_raw: print(f"{dt_object} - {modbus_packet}")
        if modbus_packet['exit_now']:
            print("Got exit condition")
            exit(1)

    #print(modbus_packet)
    if len(inverter_definitions) > 0:
        ret = decode_inverter_registers(modbus_packet,query_reference_number)



def capture_traffic(config):
    global interesting_response_transaction, last_transaction_id, query_reference_number
    global inverter_definitions
    global debug_raw
    global last_packet_timestamp
    global last_publish

    last_publish = 0
    if 'debug_raw' in config:
        debug_raw = config['debug_raw']
    else:
        debug_raw = 0

    query_reference_number = -1
    last_transaction_id = -1
    interesting_response_transaction = -1
    if 'pcap_filter' in config:
        bpf_filter = config['pcap_filter']
    else:
        bpf_filter = "(port 502 or port 1502)"
    print("Capturing packets on", config['capture_interface'], "with filter",bpf_filter)

    last_packet_timestamp = time.time()
    while True:
        try:
            network_ok = True
            sniff(iface=config['capture_interface'], prn=pkt_callback, filter=bpf_filter, store=0)
            while network_ok:
                timestamp = time.time()
                time.sleep(1)
                if int(timestamp) > int(last_packet_timestamp + 2):
                    network_ok = False
                    print("Hmm, waiting for packets on", config['capture_interface'])
        except Exception as err:
            print("ERROR while sniffing",err)
            print("Trying to restart")
            time.sleep(2)


def publish(client, msg_string, topic): 
    return client.publish(topic, msg_string)


def mqtt_on_publish(mqtt_client, userdata, result):
    if 0: print("Data published counter:",result)


def mqtt_on_connect(client, userdata, flags, rc):
    if rc != 0:
        print("MQTT connection problem")
        client.connected_flag = False
    else:
        print("MQTT client connected:" + str(client))
        client.connected_flag = True


def read_config_from_file(filename):
    config = {}
    print("Reading from:", filename)
    try:
        with open(filename, 'r') as file:
            config = yaml.safe_load(file)
    except Exception as error:
        print("ERROR: No config file? - ",error)
        exit(1)
    return config


def load_inverter_definitions(inverter_type):
    config = {}
    filename = inverter_type + ".yaml"
    print("Reading from:", filename)
    try:
        with open(filename, 'r') as file:
            config = yaml.safe_load(file)
    except Exception as error:
        print("ERROR: No definitions file? - ",error)
        exit(1)
    return config


def start_parser():
    global mqtt_client1, mqtt_client_smarthome, mqtt_client_smarthome_client_id
    global inverter_definitions
    global registers_json
    global config
    global last_json_timestamp

    last_json_timestamp = 0

    parser = argparse.ArgumentParser(description='PCAP reader')
    parser.add_argument('--pcap', metavar='<pcap file name>',
                        help='pcap file to parse', required=False)
    parser.add_argument('--filter', metavar='<reference_number>',
                        help='filter out a specific reference_number/register', required=False)
    parser.add_argument('--decode_inverter', metavar='<inverter type>',
                        help='decode inverter registers', required=False)
    parser.add_argument('--config', metavar="<filename>", 
                        help='reads config from file', required=False)
    parser.add_argument('--debug_raw', action="store_true", 
                        help='debugging deluxe', required=False)

    args = parser.parse_args()
    config = {}
    if args.config:
        print(args.config)
        config = read_config_from_file(args.config) 
    if args.pcap:
        config['pcap'] = args.pcap
    if args.filter:
        config['filter'] = args.filter
    if args.decode_inverter:
        config['decode_inverter'] = args.decode_inverter
    if args.debug_raw:
        config['debug_raw'] = args.debug_raw


    if 'decode_inverter' in config:
        inverter_definitions = load_inverter_definitions(config['decode_inverter'])
    else:
        inverter_definitions = {}

    if 'mqtt_client_id' in config:
        registers_json = {'name':config['mqtt_client_id']}
    else:
        registers_json = {}

    #print(config)
    #exit(1)

    if 'mqtt_broker' in config:
        mqtt_client1 = paho.Client(client_id=config['mqtt_client_id'], transport="tcp", protocol=paho.MQTTv311, clean_session=True)
        mqtt_client1.on_publish = mqtt_on_publish
        mqtt_client1.on_connect = mqtt_on_connect
        mqtt_client1.username_pw_set(config['mqtt_username'], config['mqtt_password'])
        mqtt_client1.connect(config['mqtt_broker'], 1883, keepalive=60)
        mqtt_client1.loop_start()
        print(f"Will publish MQTT to {config['mqtt_broker']}")

    if 'mqtt_smarthome_broker' in config:
        mqtt_client_smarthome_client_id = config['mqtt_smarthome_client_id']
        mqtt_client_smarthome = paho.Client(client_id=config['mqtt_smarthome_client_id'], transport="tcp", protocol=paho.MQTTv311, clean_session=True)
        mqtt_client_smarthome.on_publish = mqtt_on_publish
        mqtt_client_smarthome.on_connect = mqtt_on_connect
        mqtt_client_smarthome.username_pw_set(config['mqtt_smarthome_username'], config['mqtt_smarthome_password'])
        mqtt_client_smarthome.connect(config['mqtt_smarthome_broker'], config['mqtt_smarthome_port'], keepalive=60)
        mqtt_client_smarthome.loop_start()
        print(f"Will publish MQTT to {config['mqtt_smarthome_broker']}")
        print

    if 'pcap' in config:
        if not os.path.isfile(config['pcap']):
            print('"{}" does not exist'.format(config['pcap']), file=sys.stderr)
            exit(1)
        process_pcap(config)
    if 'capture_interface' in config:
        capture_traffic(config)

    exit(0)


if __name__ == '__main__':
    start_parser()

# Example:
# 1. parse-modbus.py --config
# 2. python3 parse-modbus.py --capture enxa0cec8a2b4ec --decode_inverter sungrow --inverter_ip 192.168.5.10 --mqtt_broker 192.168.5.20 --mqtt_username batt2gen24 --mqtt_password batt2gen24_pass --mqtt_client_id cm10_sungrow_1

# 2024-03-17 19:25:36.194883 - {'pkt_type': 'response', 'transaction_id': 57467, 'protocol_id': 0, 'length': 6, 'unit_id': 1, 'function_code': 6, 'exit_now': False, 'print_this_pkt': True}                                                                                     
# Registers(all): {"name":"Sungrow_SH10RT_1","timetamp":"1710699936","Export power":-72,"PV power":0,"S/N":"A232026MASK","Battery temperature":5.0,"Rated power":10000.0,"Battery capacity":24000.0,"System state":"Running in External EMS mode","Running State":"No power generated from PV|Not charging|Not Discharging|Load is active|No power feed-in the grid|Importing power from grid|No power generated from load|","Battery power":0,"Battery SoC":60.0,"Battery SoH":63.0,"Battery forced charge discharge cmd":204,"EMS mode":4,"Battery forced charge discharge power":0,"EMS Hearbeat":20}    
