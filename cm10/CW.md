# Checkwatt test

![CW test 1](./cw-test-1.png "CW test 1")

![CW test 2](./cw-test-2.png "CW test 2")

![CW testcykel](./cw-testcykel.png "testcykel")

## Tider

- Håller SoC 60% till kl 11.00
- 11.00, laddar upp till 100% med 2.4kW 0.25% av (anmäld effekt?)
- 20.45, laddar upp till 100% med 7.2kW
- 21.10, laddar upp till 100% med 2.4kW
- 21.15, laddar ur med 7.2kW (45 min ~ 5kWh)
- 22.00, laddar med 1.72kW
- 22.40, laddar med 190W
- 22.45, laddar ur med 190W
- 22.48, laddar ur med 1.72kW
- 22.49, laddar ur med 190W
- 23.40, laddar med 90W
- 23.41, laddar ur med 9.6kW
- 23.46, laddar med 90W
- 23.48, laddar med 9.6kW
- 23.53, laddar med 90W
- 23.58, klar

## Detektera test
- Kolla värden under 11.10-11.15. Om SoC>60 och control=2400 => under_test=1
- Om under_test=1: låt VPP styra helt, låt SoC åka igenom oförändrat (NJA!!) -> konfa soc_min,soc_max?
- Om under_test=0: låt VPP styra bara när den inte styr "stop", fejka soc=60% (+-0.1 random), om inte soc ligger nära min/max

## Under test, skala SoC -> CW!!!
- Fejka Batteri: 24kWh, SoH 51% (12kWh)
- 72kWh 15-85% SoC => SG 50kWh (0-100)
- SG SoC 70% ( 35kWh) vid start av period => SG 58-82% <> CW 0-100%

## Fejka power när det är stop
