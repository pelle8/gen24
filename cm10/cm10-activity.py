#!/usr/bin/env python
'''
Listens for arpscans and modbus packets, sends mqtt message
2024 Per Carlen
'''

import argparse
import os
import sys
import binascii
import yaml
import json
import time

import paho.mqtt.client as paho
from datetime import datetime
from scapy.all import sniff
from scapy.utils import RawPcapReader
from scapy.layers.l2 import Ether
from scapy.layers.inet import IP, TCP



def publish_topic(s):
    global mqtt_client_smarthome

    json_string = '{ "activity": "' + s + '"}'
    rc = publish(mqtt_client_smarthome, json_string, "network/activity")


def pkt_callback(pkt):
    global interesting_response_transaction, query_reference_number
    global last_packet_modbus
    global last_seen_arp
    global arp_list

    timestamp = time.time()

    #print(pkt)
    ether_pkt = pkt[Ether]
    if 'type' in ether_pkt.fields:
        if ether_pkt.type == 0x0800: # IP
            if pkt[IP].proto == 6: # TCP
                if pkt[IP][TCP].dport == 502:
                    print("Modbus:",pkt[IP][TCP],last_packet_modbus,timestamp)
                    if last_packet_modbus + 10 < timestamp:
                        publish_topic("Modbus activity!")
                        time.sleep(1)
                        publish_topic("Silence")
                    last_packet_modbus = time.time()

        if ether_pkt.type == 0x0806: # ARP
            if last_seen_arp + 10 < timestamp:
                arp_list = []
            else:
                if not pkt[Ether].pdst in arp_list:
                    arp_list.append(pkt[Ether].pdst)
                    if len(arp_list) == 5 or len(arp_list) == 10:
                        print("ARP scanning!")
                        publish_topic("ARP scanning!")
                        time.sleep(1)
                        publish_topic("Silence")
            last_seen_arp = time.time()

    return



def capture_traffic(config):
    global interesting_response_transaction, last_transaction_id, query_reference_number
    global inverter_definitions
    global debug_raw
    global last_packet_modbus
    global last_seen_arp
    global arp_list
    
    arp_list = []
    publish_topic("Silence")

    last_packet_modbus = 0
    last_seen_arp = 0
    if 'debug_raw' in config:
        debug_raw = config['debug_raw']
    else:
        debug_raw = 0

    bpf_filter = '(host 192.168.5.1 && host 192.168.5.20) or arp'

    print("Capturing packets on", config['capture_interface'], "with filter",bpf_filter)

    last_packet_timestamp = time.time()
    while True:
        try:
            network_ok = True
            sniff(iface=config['capture_interface'], prn=pkt_callback, filter=bpf_filter, store=0)
            while network_ok:
                timestamp = time.time()
                time.sleep(1)
                if int(timestamp) > int(last_packet_timestamp + 2):
                    network_ok = False
                    print("Hmm, waiting for packets on", config['capture_interface'])
        except:
            print("ERROR while sniffing")
            print("Trying to restart")
            time.sleep(2)


def publish(client, msg_string, topic):
    ret = client.publish(topic, msg_string)


def mqtt_on_publish(mqtt_client, userdata, result):
    if 0: print("Data published counter:",result)


def mqtt_on_connect(client, userdata, flags, rc):
    if rc != 0:
        print("MQTT connection problem")
        client.connected_flag = False
    else:
        print("MQTT client connected:" + str(client))
        client.connected_flag = True


def read_config_from_file(filename):
    config = {}
    print("Reading from:", filename)
    try:
        with open(filename, 'r') as file:
            config = yaml.safe_load(file)
    except Exception as error:
        print("ERROR: No config file? - ",error)
        exit(1)
    return config


def start_me():
    global mqtt_client1, mqtt_client_smarthome, mqtt_client_smarthome_client_id
    global registers_json
    global config

    config = read_config_from_file("cm10-activity.yaml") 

    if 'mqtt_smarthome_broker' in config:
        mqtt_client_smarthome_client_id = config['mqtt_smarthome_client_id']
        mqtt_client_smarthome = paho.Client(client_id=config['mqtt_smarthome_client_id'], transport="tcp", protocol=paho.MQTTv311, clean_session=True)
        mqtt_client_smarthome.on_publish = mqtt_on_publish
        mqtt_client_smarthome.on_connect = mqtt_on_connect
        mqtt_client_smarthome.username_pw_set(config['mqtt_smarthome_username'], config['mqtt_smarthome_password'])
        mqtt_client_smarthome.connect(config['mqtt_smarthome_broker'], config['mqtt_smarthome_port'], keepalive=60)
        mqtt_client_smarthome.loop_start()
        print(f"Will publish MQTT to {config['mqtt_smarthome_broker']}")
        print

    capture_traffic(config)

    exit(0)


if __name__ == '__main__':
    start_me()

