#!/usr/bin/env python
"""
Pymodbus Server With Updating Thread
- CM10 proxy // Per Carlen
"""
import asyncio
import binascii
import json
import math
import os
import re
import sys
import time
import copy
import syslog
import random
import paho.mqtt.client as paho
import yaml
import argparse
import curses
from curses import wrapper

# --------------------------------------------------------------------------- #
# import the modbus libraries we need
# --------------------------------------------------------------------------- #
from pymodbus.datastore import ModbusSlaveContext, ModbusServerContext, ModbusSparseDataBlock, ModbusSequentialDataBlock
from pymodbus.device import ModbusDeviceIdentification
from pymodbus.server import StartAsyncSerialServer, StartAsyncTcpServer, ServerAsyncStop
from pymodbus.transaction import ModbusRtuFramer

# --------------------------------------------------------------------------- #
# some global vars
# --------------------------------------------------------------------------- #
queue_holding = asyncio.Queue()
debug = 0

inverter_timeout = 120
last_updated = 0


def read_config_file():
    config_json = ""
    filename = "cm10-proxy.yaml"
    print("reading from:" + filename)
    try:
        path = "/etc/cm10-proxy/"
        if os.path.isfile(path + filename):
            filename = path + filename
        with open(filename, 'r') as file:
            config_json = json.dumps(yaml.safe_load(file))
    except:
        print("ERROR: No config file - " + filename)
        exit(1)

    return config_json


# --------------------------------------------------------------------------- #
# configure logging
# --------------------------------------------------------------------------- #
import logging
if debug:
    logging.basicConfig()
    log = logging.getLogger()
    log.setLevel(logging.DEBUG)

class CallbackDataBlock(ModbusSequentialDataBlock):
    """A datablock that stores the new value in memory,

    and passes the operation to a message queue for further processing.
    """

    def __init__(self, queue, addr, values):
        """Initialize."""
        self.queue = queue
        super().__init__(addr, values)


    def setValues(self, address, value):
        global config
        global vpp_control

        """Set the requested values of the datastore."""
        super().setValues(address, value)
        txt = f"Callback from setValues with address {address}, value {value}"
        # Need to send control to inverter_controller
        v1 = ""
        v2 = ""
        send_control = False
        if int(address) == 13051:
            v1 = str(value[0])
            if len(value) > 1:
                v2 = str(value[1])
        if int(address) == 13052:
            v2 = str(value[0])
        if v1 != "":
            # CM10 always sends power directly after command, let's wait for power until sending mqtt
            # send_control = True
            if v1 == "170":
                vpp_control['cmd'] = "charge"
            if v1 == "187":
                vpp_control['cmd'] = "discharge"
            if v1 == "204":
                vpp_control['cmd'] = "stop"
        if v2 != "":
            send_control = True
            if config['multiple_proxies']:
                vpp_control['power'] = len(config['inverters']) * int(v2)
            else: 
                vpp_control['power'] = int(v2)
            ts = str(int(time.time()))
            print_win("vpp",ts + " - VPP Command from CM10:" + str(vpp_control['cmd']) + "," + str(int(v2)) + "W, multiple proxies:" + str(config['multiple_proxies']) +  ", power transformed:" + str(vpp_control['power']) + "\n")
        if config['debug']: print_win("log",txt + "\n")
        if send_control:
            send_control_message(vpp_control)


    def getValues(self, address, count=1):
        global last_inverter_read
        global config

        last_inverter_read = str(int(time.time()))
        """Return the requested values from the datastore."""
        result = super().getValues(address, count=count)
        txt = f"Callback from getValues with address {address}, count {count}, data {result}"
        if config['debug']: print_win("log",txt + "\n")
        return result

    def validate(self, address, count=1):
        """Check to see if the request is in range."""
        result = super().validate(address, count=count)
        txt = f"Callback from validate with address {address}, count {count}, data {result}"
        return result


def get_power_distribution(vpp_control) -> dict:
    global config
    global first_inverter_is_king

    control_dict = {}
    if 'max_power' in config['inverters'][0]:
        if int(config['inverters'][0]['max_power']) > int(vpp_control['power']):
            first_inverter_is_king = True
            for idx,i in enumerate(config['inverters']):
                if idx == 0:
                    control_dict.update( { i['name']: vpp_control } )
                else:
                    control_dict.update({i['name']: {'cmd': 'stop', 'power': 0 }})
        else:
            first_inverter_is_king = False
            power_shared = int(int(vpp_control['power']) / len(config['inverters']))
            for idx,i in enumerate(config['inverters']):
                control_dict.update({i['name']: {'cmd': vpp_control['cmd'], 'power': power_shared }})
    else:
        print_win("log","Missing max_power in inverter configuration\n")
    return control_dict


def send_control_message(vpp_control):
    global mqtt_client_pub
    global config

    if float(config['power_multiplier']) != 1: # Increase/decrease the requested power...
        new_power = int(int(vpp_control['power']) * float(config['power_multiplier']))
        vpp_control['power'] = new_power

    control_dict = get_power_distribution(vpp_control)
    vpp_control = {**vpp_control, **control_dict} # Include both old fashion vpp control and inverter_specific
    ts = str(int(time.time()))
    if config['mqtt_publish']:
        print_win("log","Will publish MQTT:" + str(vpp_control['cmd']) + " " + str(vpp_control['power']) + "\n")
        for idx, inverter in enumerate(config['inverters']):
            print_win("control"," " + inverter['name'] + ": " + str(vpp_control[inverter['name']]) + ", power multiplier:" + str(config['power_multiplier']) + "\n")

        rc = publish(mqtt_client_pub, vpp_control, "vpp/control")
    else:

        print_win("log", ts + " - Will not publish MQTT, since it's not enabled in config\n")


def init_datastore(queue_holding,inverter_interface_type, modbus_address):
    # ----------------------------------------------------------------------- # 
    # initialize your data store
    # ----------------------------------------------------------------------- # 

    if re.search(r'Sungrow', inverter_interface_type): 
        print_win("log","Init Sungrow datastore\n")
        r4989_input = [1] * 6
        r5016_input = [0] * 2
        r5627_input = [0] 
        r12999_input = [0] 
        r13000_input = [0] 
        r13009_input = [0] 
        r13021_input = [0] 
        r13022_input = [0] 
        r13023_input = [0] 
        r13024_input = [0] 
        r13038_input = [0] 
        r13049_holding = [0] 
        r13050_holding = [0] 
        r13051_holding = [0] 
        r13079_holding = [0] 
        block_input = ModbusSparseDataBlock( { 4990: [1] * 30, 5628: [0], 13000: [0] * 40   } )
        block_holding = CallbackDataBlock(queue_holding, 0x00, [0] * 13400 )
        block = ModbusSparseDataBlock()
        store = ModbusSlaveContext(di=block, co=block, hr=block_holding, ir=block_input, unit=modbus_address)
        context = ModbusServerContext(slaves=store, single=True)

    return context


def save_power(json_data):
    global list_power
    global list_pv_power
    global config

    for idx, inverter in enumerate(config['inverters']):
        if json_data['name'] == inverter['name']:
            batt_power = int(json_data['Battery power'])
            list_power[idx] = batt_power
            pv_power = int(json_data['PV power'])
            list_pv_power[idx] = pv_power


def mqtt_on_message(mqtt_client, userdata, message, tmp=None):
    global context
    global config

    try:
        json_data = json.loads(message.payload)
        if type(json_data) is dict or type(json_data) is list:
            if 'name' in json_data:
                save_power(json_data)

                # For now, one inverter is master, from which all values are taken
                if json_data['name'] == config['fake_inverter']['name']:
                    if config['debug']: print_win("log"," Received message " + str(message.payload) + "\n")
                    check_registers(context,json_data)
    except Exception as err:
        print_win("log","Unexpected error:" + str(err))
        pass


def mqtt_on_publish(mqtt_client, userdata, result):
  pass


def publish(mqtt_client, pub_var, topic):
  global config

  json_string = json.dumps(pub_var)
  ret = mqtt_client.publish(topic, json_string)
  if config['debug']: print_win("log","Publishing in:" + topic + ":" + json_string + "\n")


def mqtt_on_subscribe(client, userdata, mid, granted_qos):
    pass


def mqtt_on_connect(client, userdata, flags, rc):

    if rc != 0:
        print_win("log","MQTT connection problem" + "\n")
        client.connected_flag = False
    else:
        print_win("log","MQTT client connected:" + str(client) + "\n")
        client.connected_flag = True
        client.subscribe("inverter/monitor", 2)


def fake_the_soc(soc_from_inverter,fake_soc,soc_low,soc_high) -> float:
    # No idea to fake soc during test, since cm10 won't care. The inverter needs to handle that.

    # Fake soc 
    soc = int(fake_soc) * 10 #- 2 + random.randint(1, 3) 
    #soc = 590
    if soc_from_inverter < int(soc_low):
        soc = 10* int(fake_soc) * (soc_from_inverter/int(soc_low))
    if soc_from_inverter > int(soc_high):
        soc = 10 * int(fake_soc) + (1000 - 10 * int(fake_soc)) * (soc_from_inverter - int(soc_high)) / (100 - int(soc_high))
    soc = soc * 0.1
    return soc


# --------------------------------------------------------------------------- #
# updates the registers from mqtt data
# --------------------------------------------------------------------------- #

def check_registers(context,json_data):
    global inverter_timeout
    global debug
    global last_updated
    global inverter_interface_type
    global modbus_data
    global modbus_data_previous
    global vpp_control
    global config
    global batt_power_counter
    global batt_power
    global list_power
    global list_pv_power
    global first_inverter_is_king

    slave_id = 0
    timestamp_now = int(time.time())
    registers = json_data['inverter_registers']

    total_power = 0
    total_pv_power = 0
    for p in list_power:
        total_power += p
    if first_inverter_is_king:       # Only feedback the power of the first inverter if it handles all
        total_power = list_power[0]

    for p in list_pv_power:
        total_pv_power += p

    if config['multiple_proxies']:
        total_pv_power = int(total_pv_power / len(config['inverters']))

    pv_register = [total_pv_power,0]

    print_win("inverter","Total PV power:" + str(total_pv_power) + ", details:" + str(list_pv_power) + "\n")

    if config['fake_inverter']['type'] == "Sungrow":
        context[slave_id].setValues(4, 4989,registers['4989-input'])
        context[slave_id].setValues(4, 5016,pv_register)
        context[slave_id].setValues(4, 5627,registers['5627-input'])
        context[slave_id].setValues(4, 12999,registers['12999-input'])
        context[slave_id].setValues(4, 13009,registers['13009-input'])
        context[slave_id].setValues(4, 13024,registers['13024-input'])

        running_state = int(registers['13000-input'][0])
        # Only feed back power value when not stopped, and fake the running_state bits (remove charge/discharge bits)
        if vpp_control['cmd'] != "stop":
            batt_power = abs(total_power)
            if batt_power > 1.5 * vpp_control['power']: # Tune down any leftovers from self optimization...
                batt_power = int(0.01 * random.randint(100, 150) * vpp_control['power'])  #100-150% of the wanted power
                print_win("log","LIMITING " + str(total_power) + " to:" + str(batt_power) + ", VPP:" + str(vpp_control['power']) + "\n")
            if config['multiple_proxies']:
                batt_power = int(batt_power / len(config['inverters']))

        else:   # Randomize the batt_power a bit, make it look like a real one
            running_state = running_state & (255 - 2 - 4)

            if batt_power_counter > 0.10:
                batt_power_counter = 0
                r = int(random.randint(0, 10))
                if r > 5:
                    batt_power = int(random.randint(0, 140))
                else: 
                    batt_power = 0
            else:
                batt_power_counter += 0.01
                batt_power += batt_power_counter

        print_win("inverter","Total Batt Power:" + str(int(batt_power))  +", details:" + str(list_power) + "\n")
        context[slave_id].setValues(4, 13021,[int(batt_power)])
        context[slave_id].setValues(4, 13000,[running_state])
        
        # Fake soh
        soh = 10 * int(config['fake_inverter']['battery_soh']) 
        context[slave_id].setValues(4, 13023,[soh])

        # Fake battery size...
        context[slave_id].setValues(4, 13038,[int(config['fake_inverter']['battery_capacity']/100)] )
    
        soc = 10 * fake_the_soc(float(json_data['Battery SoC']),int(config['fake_inverter']['fake_soc']),int(config['fake_inverter']['soc_low']),
          int(config['fake_inverter']['soc_high']))
        #soc = 610
        print_win("inverter","SoC reported by SG:" + str(json_data['Battery SoC']) + ", SoC repoted to CM10:" + str(int(soc/10)) + "\n")     
        context[slave_id].setValues(4, 13022,[int(soc)])

    ts = str(int(time.time()))
    print_win("vpp", ts + " - VPP power distributed to " + str(len(config['inverters'])) + " inverter(s): " + str(vpp_control['power']) + "W" + ", power multiplier:" + str(config['power_multiplier']) + "\n")


    
def run_updating_server(queue_holding):
    global debug
    global inverter_timeout
    global context
    global inverter_interface_type
    global fake_topic
    global meter_same_data_counter
    global modbus_data,modbus_data_previous
    global modbus_same_data_counter
    global config
    global vpp_control
    global mqtt_client_sub,mqtt_client_pub
    global batt_power_counter
    global batt_power
    global total_power
    global list_power
    global list_pv_power
    global first_inverter_is_king

    if config:
        inverter_timeout = config['inverter_timeout']
        debug = config['debug']
        mqtt_broker = config['mqtt_broker']
        mqtt_port = config['mqtt_port']
        mqtt_username = config['mqtt_username']
        mqtt_password = config['mqtt_password']
    else:
        exit(1)
    inverter_interface = config['fake_inverter_ip_port']
    vpp_control = {'cmd':'stop', 'power':0}
    modbus_data = 0
    meter_same_data_counter = 0
    modbus_same_data_counter = 0
    batt_power_counter = 0
    batt_power = 0
    first_inverter_is_king = True
    random_string = str(binascii.b2a_hex(os.urandom(8)))

    print_win("log","Will distribbute power to " + str(len(config['inverters'])) + " inverter(s)" + "\n")
    list_power = [0]  * len(config['inverters']) 
    list_pv_power = [0]  * len(config['inverters']) 

    # this mqtt client will publish
    mqtt_client_pub = paho.Client(client_id="cm10_proxy_publisher_" + random_string, transport="tcp", protocol=paho.MQTTv311,
                                clean_session=True)
    mqtt_client_pub.on_publish = mqtt_on_publish
    mqtt_client_pub.on_connect = mqtt_on_connect
    mqtt_client_pub.username_pw_set(mqtt_username, mqtt_password)
    mqtt_client_pub.connect(mqtt_broker, mqtt_port, keepalive=60)
    mqtt_client_pub.loop_start()

    # this mqtt client will subscribe
    mqtt_client_sub = paho.Client(client_id="cm10_proxy_subscriber_" + random_string, transport="tcp",
                               protocol=paho.MQTTv311, clean_session=True)
    mqtt_client_sub.on_subscribe = mqtt_on_subscribe
    mqtt_client_sub.on_connect = mqtt_on_connect
    mqtt_client_sub.on_message = mqtt_on_message
    mqtt_client_sub.username_pw_set(mqtt_username, mqtt_password)
    mqtt_client_sub.connect(mqtt_broker, mqtt_port, keepalive=60)
    mqtt_client_sub.loop_start()

    context = init_datastore(queue_holding,config['fake_inverter']['type'],config['fake_inverter']['modbus_address'])

    # ----------------------------------------------------------------------- # 
    # initialize the server information
    # ----------------------------------------------------------------------- # 
    identity = ModbusDeviceIdentification()
    identity.VendorName = 'pymodbus'
    identity.ProductCode = 'PM'
    identity.VendorUrl = 'http://github.com/riptideio/pymodbus/'
    identity.ProductName = 'pymodbus Server'
    identity.ModelName = 'pymodbus Server'

    print_win("modbus","Modbus server on " + inverter_interface + "\n")
    asyncio.run(run_async_server(context, identity, config['fake_inverter']['type'], inverter_interface))
    syslog.syslog(syslog.LOG_INFO, "Modbus server ended")
    print_win("log","Server ended" + "\n")


def test():
    config = json.loads(read_config_file())

    json_data={}
    print_win("log","SoC:")
    for soc_inverter in range(1001):
        json_data['Battery SoC'] = soc_inverter*0.1
        soc = fake_the_soc(json_data['Battery SoC'],int(config['fake_inverter']['fake_soc']),config['fake_inverter']['soc_low'],config['fake_inverter']['soc_high'])
        print_win("log","SoC reported by inverter:" + str(int(soc_inverter/10)) + ", SoC repoted to CM10:",str(soc) + "\n")


async def run_async_server(context, identity, inverter_interface_type, inverter_interface):
    print_win("log","Register task for modbus server" + "\n")
    #asyncio.create_task(check_meter_data())
    inverter_ip = inverter_interface.split(":")[0]
    inverter_port = int(inverter_interface.split(":")[1])
    print_win("log"," Modbus tcp server at: " + str(inverter_ip) + ":" + str(inverter_port) + "\n")
    try:
        await StartAsyncTcpServer(context=context, identity=identity, address=(inverter_ip, inverter_port))
    except KeyboardInterrupt:
        pass  

    print_win("log","\n\nCleaning up, please be patient" + "\n")
    await ServerAsyncStop()
    mqtt_client_sub.disconnect()
    mqtt_client_pub.disconnect()
    print_win("log","DONE\n\n\n")


def print_win(window_name,text):
  global window,win_modbus,win_log,win_vpp,win_inverter,win_control
  global config

  if config['use_curses']:
    if window_name == "modbus":
      rows, cols = win_modbus.getmaxyx()
      win_modbus.addstr(1,1,text + " " * (cols-len(text) - 3))
      win_modbus.refresh()

    if window_name == "inverter":
      rows, cols = win_inverter.getmaxyx()
      y, x = win_inverter.getyx()
      win_inverter.addstr(y,1,text + " " * (cols-len(text) - 3))
      win_inverter.refresh()

    if window_name == "control":
      rows, cols = win_control.getmaxyx()
      y, x = win_control.getyx()
      win_control.addstr(y,1,text + " " * (cols-len(text) - 3))
      win_control.refresh()

    if window_name == "log":
      rows, cols = win_log.getmaxyx()
      y, x = win_log.getyx()
      win_log.addstr(y,1,text + " " * (cols-len(text) - 3))
      win_log.refresh()

    if window_name == "vpp":
      rows, cols = win_vpp.getmaxyx()
      y, x = win_vpp.getyx()
      win_vpp.addstr(y,1 ,text + " " * (cols-len(text) - 3))
      win_vpp.refresh()
  else:
    if re.search(".*\n$",text):
      print(text,end='')
    else:
      print(text)


def windows_update():
  global window,win_modbus,win_log,win_control,win_vpp,win_inverter

  curses.resizeterm(*window.getmaxyx())
  window.refresh()
  print_win("log","Updated curses\n")

def start_windows(stdscr) -> int:
    global window,win_modbus,win_log,win_control,win_vpp,win_inverter

    stdscr = curses.initscr()
    stdscr.clear()
    window = stdscr
    rows, cols = stdscr.getmaxyx()
    if rows < 35 or cols < 100:
        return rows, cols
    curses.curs_set(0)
    rows, cols = window.getmaxyx()
    window.addstr(0,0,"─── Modbus " + "─" *  (cols - 13))
    window.addstr(3,0,"─── VPP " + "─" *  (cols - 10))
    window.addstr(15,0,"─── Inverter " + "─" * 60)
    window.addstr(15,70,"─── Control " + "─" * 80)
    window.addstr(28,0,"─── Log " + "─" *  (cols - 10))
    window.refresh()
    win_modbus = stdscr.subwin(3,cols,0,0)
    win_vpp = stdscr.subwin(11,cols,4,0)
    win_inverter = stdscr.subwin(12,69,16,0)
    win_control = stdscr.subwin(12,cols-70,16,70)
    win_log = stdscr.subwin(rows-29,cols,29,0)
    win_log.scrollok(True)
    win_inverter.scrollok(True)
    win_control.scrollok(True)
    win_vpp.scrollok(True)
    windows_update()
    run_updating_server(queue_holding)
    return 0

def startmeup():
    global config

    print("\nStarting cm10 proxy")
    config = json.loads(read_config_file())
    print(config)
    if 'use_curses' in config:
        if config['use_curses']:
            rows, cols = wrapper(start_windows)
            if rows > 0:
                print(rows,"x",cols)
                print("Too small window to run this in curses!")
                exit(1)

    run_updating_server(queue_holding)



if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='CM10 Proxy')
    parser.add_argument('--test', action="store_true", 
                        help='do some tests', required=False)
    args = parser.parse_args()
    if args.test:
        test()
        exit(1)
    startmeup()
