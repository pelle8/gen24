# CM10 monitor

So, you lost control of your inverter, to a CM10?

Don't worry - get an RPi, a switch that can do port mirroring and maybe a USB NIC for the mirrored traffic. 
Use the script in this repo to decode and send the data to Home Assistant etc.

## Physical

- Configure switch to mirror the traffic on the port that the CM10 is hooked up to, send this traffic to a port where the RPI is connected.


## Script

There is only one script at the moment, cm10-monitor.py. This can do live monitoring and parse saved pcap files.
Since the script is doing packet capturing, you need to run the script privileged. The simplest method to do this is described here, just google and you will probably find better ways.

- Put yourself in the cm10 path
- Install the requirements with: `sudo pip install -r requirements.txt`
- Copy the example config-file: `cp config.yaml.example config.yaml`
- Edit the config-file to suite your setup
- Run live monitoring: `sudo python3 cm10-monitor.py --config`
- Parse a saved pcap: `python3 cm10-monitor.py --pcap /home/pelle/1.pcap --decode_inverter sungrow`


## Decoders

Currently the following inverters have decoders:

- Sungrow

Other inverters can be added by writing a "decoder"-file. Check out sungrow.yaml for reference.


## Example MQTT message

```
{"name":"Sungrow_SH10RT_1","timetamp":"1710602866","Export power":-3384,"PV power":128,"S/N":"A232026MASK","Battery temperature":11.0,"Rated power":10000.0,"Battery capacity":24000.0,"System state":"Running in External EMS mode","Running State":"Power generated from PV|Not charging|Not Discharging|Load is active|No power feed-in the grid|Importing power from grid|No power generated from load|","Battery power":152,"Battery SoC":60.0,"Battery SoH":63.0,"Battery forced charge discharge cmd":204,"EMS mode":4,"Battery forced charge discharge power":0,"EMS Hearbeat":20}
```
