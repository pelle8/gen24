#!/usr/bin/env python
'''
Pymodbus Synchronous Client Examples
--------------------------------------------------------------------------

The following is an example of how to use the synchronous modbus client
implementation from pymodbus.

Modified to connect to Fronius Gen24, Per Carlen
'''
from pymodbus.client import ModbusTcpClient as ModbusClient

import logging
#logging.basicConfig()
#log = logging.getLogger()
#log.setLevel(logging.DEBUG)


def print_rr(lbl,rr):
  print(lbl + ":",end='')
  for x in rr.registers:
    s = x.to_bytes(2,"big").hex()
    print(s + " (" + str(x) + ")",end='')
  print("")


def sungrow():
  client = ModbusClient('192.168.1.201', port=502)

# discharge 500W:
  registers = [0xbb,500]
  rr = client.write_registers(13049, 2, 0x01)
  rr = client.write_registers(13050, registers, 0x01)   #aa bb cc charge/disch/stop

#  registers = [65535,65535]
#  rr = client.write_registers(13049, 0, 0x01)
#  rr = client.write_registers(13050, registers, 0x01)   #aa bb cc charge/disch/stop

#  rr = client.write_registers(13050, registers, 0x01)   #aa bb cc charge/disch/stop
  rr = client.read_holding_registers( 13048, 10, 1)
  print(rr.registers)


#  rr = client.read_input_registers( 5599, 40, 1)
#  print(rr.registers)

  client.close()

def fronius():
  client = ModbusClient('192.168.1.202', port=502)
  client.connect()
#  rr = client.write_registers(40345, 10000, 0x01) # 

  rr = client.read_holding_registers(40345, 1, 0x01)
  print_rr("WChamax",rr)

#  rr = client.write_registers(40350, 1100, 0x01) # soc
#  rr = client.write_registers(40355, 65534, 0x01) # outrte 
##  rr = client.write_registers(40356, 65535, 0x01) # inwrte 
#  rr = client.write_registers(40348, 3, 0x01) # Charge limit
#  rr = client.write_registers(40232, 1000, 0x01) 

#  rr = client.write_registers(40348, 3, 0x01) # Charge limit
##  rr = client.write_registers(40355, 1000, 0x01) # outrte (disch)
#  rr = client.write_registers(40356, 64536, 0x01) # inwrte (ch)

  # Charge:
#  rr = client.write_registers(40348, 2, 0x01) # Charge limit
#  rr = client.write_registers(40355, [65170,0], 0x01) # inwrte (ch)

  # Discharge:
#  rr = client.write_registers(40348, 3, 0x01) # Charge limit
#  rr = client.write_registers(40348, 0, 0x01) # Charge limit
#  rr = client.write_registers(40355, [0,0], 0x01) # inwrte (ch)
#  rr = client.write_registers(40348, 0, 0x01) # Charge limit
#  rr = client.write_registers(40355, [0,0], 0x01) # inwrte (ch)

#  rr = client.write_registers(40348, 3, 0x01) # Charge limit
#  rr = client.write_registers(40355, 65170, 0x01) # outrte (disch)
#  rr = client.write_registers(40356, 363, 0x01) # inwrte (ch)

  rr = client.write_registers(40348, 3, 0x01) # Charge limit
  rr = client.write_registers(40355, 2007, 0x01) # Charge limit
  rr = client.write_registers(40356, 63529, 0x01) # Charge limit



  #rr = client.write_registers(40350, 500, unit=0x01) # soc 5%
#  addr = 40342
#  while addr < 40370:
  addr = 40346
  while addr < 40360:
    rr = client.read_holding_registers( addr, 1, 1)
  #  rr = client.read_holding_registers(5011 + addr, 1, 1)
    try:
      rr.registers
    except:
      print(str(addr) + ":" + str(rr))
    else:
      print_rr(str(addr+1),rr)
    addr += 1

  rr = client.read_holding_registers(40274, 70, 0x01)
  print_rr("P PV1",rr)
  print("aaa",rr.registers[60])
  rr = client.read_holding_registers(40294, 1, 0x01)
  print_rr("P PV2",rr)
  rr = client.read_holding_registers(40354, 1, 0x01)
  print_rr("ChaSt",rr)
  rr = client.read_holding_registers(40083, 2, 0x01)
  print_rr("AC POWER",rr)

  rr = client.read_holding_registers(40257, 1, 0x01)
  print_rr("SCALE",rr)
  rr = client.read_holding_registers(40351, 1, 0x01)
  print_rr("SOC",rr)

#  rr = client.write_registers(40348, 2, 0x01) # Charge limit
#  rr = client.write_registers(40355, 100, 0x01) # outrte 
#  rr = client.write_registers(40356, 0, 0x01) # inwrte 

  #rr = client.write_registers(40348, 1, unit=0x01) # disCharge limit
  #rr = client.write_registers(40355, 0, unit=0x01) # outrte 
  #rr = client.write_registers(40356, 64300, unit=0x01) # inwrte 

  #rr = client.read_holding_registers(40348, 1, unit=0x01)
  #print_rr("StorCtl_Mod",rr)
  #rr = client.read_holding_registers(40354, 1, unit=0x01)
  #print_rr("ChaSt",rr)
  #rr = client.read_holding_registers(40360, 1, unit=0x01)
  #print_rr("CHaGriSet",rr)
  #rr = client.read_holding_registers(40355, 1, unit=0x01)
  #print_rr("OutWRte",rr)
  #rr = client.read_holding_registers(40356, 1, unit=0x01)
  #print_rr("InWRte",rr)
  #rr = client.read_holding_registers(40350, 1, unit=0x01)
  #print_rr("MinRsvPct",rr)
  #rr = client.read_holding_registers(40351, 1, unit=0x01)
  #print_rr("ChaState",rr)
  #rr = client.read_holding_registers(40232, 1, unit=0x01)
  #print_rr("WMaxLimPct",rr)
  #rr = client.read_holding_registers(40107, 1, unit=0x01)
  #print_rr("St",rr)
  client.close()

#fronius()
sungrow()

