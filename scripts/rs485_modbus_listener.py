# Passive Modbus RTU monitor
# Programmed by Entropia
# Inspired by Dala's EV Repair
#
# Heavily modified by Per Carlen (wait for a period of silence before decoding frame(s)) 
#
# Will not be a generic modbus decoder, only frametypes applicable to Gen24/BYD are decoded at the moment

import serial
import struct
import datetime
import time
import sys

def HexToStr(input):
    return hex(ord(input))[2:]

def HexToInt(input):
    return int(input,16)

def IntIEEE754Decode(input):
    packed_v = struct.pack('>l', input)
    return struct.unpack('>f', packed_v)[0]

# Combine two hex strings to one 16-bit int
def Combine(input1, input2):
    temp = int(input1, base=16) << 8
    temp += int(input2, base=16)
    return temp

# Combine four hex strings to one 32-bit int
def Combine4(input1, input2, input3, input4):
    temp = int(input1, base=16) << 24
    temp += int(input2, base=16) << 16
    temp += int(input3, base=16) << 8
    temp += int(input4, base=16)
    return temp

# Compute and compare crc
def crc16(buff):
  l = len(buff)
  crc_msg = Combine(buff[l-1],buff[l-2])
  crc = 0xffff
  for x in range (0, l-2):
    b = HexToInt(buff[x])
    crc = crc ^ b #xor
    n = 0
    while n < 8:
      lsb = crc & 1
      crc = crc >> 1
      if lsb == 1:
        crc = crc ^ 0xa001
      n += 1
  ret = False
  if crc == crc_msg:
    ret = True
  return ret


# All the magic happens here
def DecodeModbusFrame(rxbuffer):
    timestamp = datetime.datetime.now()
    operation = rxbuffer[1]
    if debug > 1: print(timestamp, "(op:" , operation ,") Len:", len(rxbuffer), "Lastbyte:", rxbuffer[len(rxbuffer) - 1])
    crc_ok = crc16(rxbuffer)
    if not crc_ok:
       print("CRC mismatch, will not decode frame")
       return (False,0,0)
    
    # Let's try to see what kind of frame we have...why doesn't every frame include a length byte - would have been so much easier... 

    # Exceptions
    if (rxbuffer[1] == "83" or rxbuffer[1] == "86" or rxbuffer[1] == "90") :
            exception_codes_enum = ["","ILLEGAL FUNCTION","ILLEGAL DATA ADDRESS","ILLEGAL DATA VALUE","SLAVE DEVICE FAILURE","","","","","","","GATEWAY TARGET DEVICE FAILED TO RESPOND"]
            exception_code = HexToInt(rxbuffer[2])
            print(timestamp,"Exception:",exception_codes_enum[exception_code] )
            return True

    # Requests
    if(len(rxbuffer) > 8 and rxbuffer[1] == '10'): #Write multiple registers
            deviceID = rxbuffer[0]
            deviceID = rxbuffer[0]
            address = int(rxbuffer[2], base=16) << 8
            address += int(rxbuffer[3], base=16)
            quantity = int(rxbuffer[4], base=16) << 8
            quantity += int(rxbuffer[5], base=16)
            timestamp = datetime.datetime.now()
            print(timestamp, "Write multiple for device ID ", deviceID, ", Start address = ", address, " data = " , end='')
            for x in range (0, quantity):
                value = int(rxbuffer[x*2+7], base=16) << 8
                value += int(rxbuffer[x*2+8], base=16) 
                print(value,",", end='')
            print(" ")
            return True

    elif(len(rxbuffer) == 8 and rxbuffer[1] != '10'): # also requests
            if(rxbuffer[1] == '3'):
                # Read Holding Register
                deviceID = rxbuffer[0]
                address = int(rxbuffer[2], base=16) << 8
                address += int(rxbuffer[3], base=16)
                quantity = int(rxbuffer[4], base=16) << 8
                quantity += int(rxbuffer[5], base=16)
                timestamp = datetime.datetime.now()
                print(timestamp, "Read holding register for device ID", deviceID, ", Start address =", address, "Quantity =", quantity)
                return True
            elif(rxbuffer[1] == '4'):
                # Read Input Register
                deviceID = rxbuffer[0]
                address = Combine(rxbuffer[2], rxbuffer[3])
                quantity = Combine(rxbuffer[4], rxbuffer[5])
                timestamp = datetime.datetime.now()
                print(timestamp, "Read input register for device ID", deviceID, ", Start address =", address, "Quantity =", quantity)
                return True
            elif(rxbuffer[1] == '5'):
                # Write Single Coil
                deviceID = rxbuffer[0]
                address = Combine(rxbuffer[2], rxbuffer[3])
                quantity = Combine(rxbuffer[4], rxbuffer[5])
                timestamp = datetime.datetime.now()
                print(timestamp, "Write single coil for device ID", deviceID, ", Start address =", address, "Quantity =", quantity)
                return True
            elif(rxbuffer[1] == '6'):
                # Write Single Register
                deviceID = rxbuffer[0]
                address = Combine(rxbuffer[2], rxbuffer[3])
                quantity = Combine(rxbuffer[4], rxbuffer[5])
                timestamp = datetime.datetime.now()
                print(timestamp, "Write single register for device ID", deviceID, ", Start address =", address, "Quantity =", quantity)
                return True
    else: 
          # Responses
          if(rxbuffer[1] == '3'):
            deviceID = rxbuffer[0]
            timestamp = datetime.datetime.now()
            quantity = int(HexToInt(rxbuffer[2])/2)
            print(timestamp, "Respose from device ID", deviceID, " data(",quantity,") = " , end='')
            for x in range(0, quantity):
                data = Combine(rxbuffer[(x * 2) + 3], rxbuffer[(x * 2) + 4])
                print(data,",",end='')
            if(quantity == 2):
                print(timestamp, "Float:", IntIEEE754Decode(Combine4(rxbuffer[3], rxbuffer[4], rxbuffer[5], rxbuffer[6])))
            print(" ")
            return True
          if(rxbuffer[1] == '4'):
            deviceID = rxbuffer[0]
            timestamp = datetime.datetime.now()
            for x in range(0, quantity):
                data = Combine(rxbuffer[(x * 2) + 3], rxbuffer[(x * 2) + 4])
                print(timestamp, " * Response", (x + 1), "from device ID", deviceID, ", data =", data)
            return True
          if(rxbuffer[1] == '5'):
            deviceID = rxbuffer[0]
            address = Combine(rxbuffer[2], rxbuffer[3])
            data = Combine(rxbuffer[4], rxbuffer[5])
            timestamp = datetime.datetime.now()
            print(timestamp, "Response from device ID", deviceID, " Address", address, "is now", data)
            return True
          if(rxbuffer[1] == '6' and len(rxbuffer) == 8):
            deviceID = rxbuffer[0]
            address = Combine(rxbuffer[2], rxbuffer[3])
            data = Combine(rxbuffer[4], rxbuffer[5])
            timestamp = datetime.datetime.now()
            print(timestamp, "Response from device ID", deviceID, " Address", address, "is now", data)
            return True
          if(rxbuffer[1] == '0f'):
            deviceID = rxbuffer[quantity + 3]
            address = Combine(rxbuffer[quantity + 4], rxbuffer[quantity + 5])
            timestamp = datetime.datetime.now()
            for x in range (0, quantity):
                print(timestamp, "Write command", x, ":", rxbuffer[x])
            return True

          if(rxbuffer[1] == '10'):
                deviceID = rxbuffer[0]
                address = int(rxbuffer[2], base=16) << 8
                address += int(rxbuffer[3], base=16)
                quantity = int(rxbuffer[4], base=16) << 8
                quantity += int(rxbuffer[5], base=16)
                timestamp = datetime.datetime.now()
                print(timestamp, "Write response from device ID ", deviceID, ", Start address =", address, "Quantity =", quantity)
                quantity *= 2
                return True
          else:
            print(timestamp, "Error: couldn't decode the frame")
            return True
    print(timestamp, "Error: couldn't decode the frame, how did I get here?")
    return True

# Open serial port
ser = serial.Serial('/dev/ttyACM0', 9600)
# Set up variables for first time use
rxbuffer = []
debug = 1
testrun = 0

# Some test data...uncomment a row below, and set testrun = 1
#rxbuffer = ["28","3","9c","49","0","a","3d","b2","28","3","9c","49","0","a","3d","b2","29","3","9c","49","0","a","3c","63","29","3","9c","49","0","a","3c","63","2a","3","9c","49","0","a","3c","50","2a","3","9c","49","0","a","3c","50","2b","3","9c","49","0","a","3d","81","2b","3","9c","49","0","a","3d","81","2c","3","9c","49","0","a","3c","36","2c","3","9c","49","0","a","3c","36","2d","3","9c","49","0","a","3d","e7","2d","3","9c","49","0","a","3d","e7","2e","3","9c","49","0","a","3d","d4","2e","3","9c","49","0","a","3d","d4","2f","3","9c","49","0","a","3c","5","2f","3","9c","49","0","a","3c","5","15","3","1","2c","0","18","86","e1","15","3","30","0","3","0","0","0","80","2","bc","aa","35","b","ea","25","a6","7e","70","f","b5","0","0","f","b0","0","0","0","a0","0","be","0","0","0","0","0","65","b3","8","0","0","0","0","0","5c","89","26","0","fa","26","7a","8c","3d","15","3","3","e8","0","64","c7","45","15","3","c8","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","98","cd"]
#rxbuffer = ["15","3","1","2c","0","18","86","e1","15","3","30","0","3","0","0","0","80","8","98","5d","c0","14","a0","2","a9","10","d8","6","cb","0","0","0","0","0","0","0","1e","0","32","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","f0","26","ac","35","fc","15","3","3","e8","0","64","c7","45","15","3","c8","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","98","cd"]
#rxbuffer = ["15","3","0","64","0","2","86","c0","15","3","30","0","3","0","0","0","40","22","60","5d","c0","52","80","e","a4","10","d8","7","b2","0","7","0","0","0","7","0","1e","0","32","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","f0","26","ac","e4","8c"]
#rxbuffer = ["15","3","0","64","0","2","86","c0","15","3","4","53","49","0","1","af","60","15","3","0","66","0","10","a7","d","15","3","20","42","59","44","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","b5","89","15","3","0","76","0","10","a6","c8","15","3","20","42","59","44","20","42","61","74","74","65","72","79","2d","42","6f","78","20","50","72","65","6d","69","75","6d","20","48","56","0","0","0","0","0","0","b0","5d","15","3","0","a6","0","2","27","3c","15","3","4","0","1","0","0","ff","f2","15","3","0","64","0","44","7","32","15","3","88","53","49","0","1","42","59","44","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","42","59","44","20","42","61","74","74","65","72","79","2d","42","6f","78","20","50","72","65","6d","69","75","6d","20","48","56","0","0","0","0","0","0","35","2e","30","0","0","0","0","0","0","0","0","0","0","0","0","0","33","2e","31","36","0","0","0","0","0","0","0","0","0","0","0","0","50","65","6c","6c","65","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","1","0","0","df","e2","15","3","0","c8","0","d","6","e5","15","3","1a","0","0","0","0","5d","c0","10","d8","10","d8","7","e0","6","9e","0","85","0","a","0","85","0","a","0","0","0","0","b4","6f","15","3","1","2c","0","18","86","e1","15","3","30","0","3","0","0","0","80","22","60","5d","c0","52","80","e","a4","10","d8","7","b0","0","4","0","0","0","4","0","1e","0","32","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","f0","26","ac","bb","55","15","10","1","90","0","9","12","0","2","ff","0","0","3c","0","0","0","0","63","c3","c1","cb","0","0","0","1","ea","39","15","10","1","90","0","9","2","ca","15","3","1","2c","0","18","86","e1","15","3","30","0","3","0","0","0","80","22","60","5d","c0","52","80","e","a4","10","d8","7","b0","0","4","0","0","0","4","0","1e","0","32","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","f0","26","ac","bb","55","15","3","1","91","0","1","d7","f","15","3","2","ff","0","c9","b7","15","10","1","91","0","1","2","0","ff","15","51","15","10","1","91","0","1","52","cc","15","3","3","e8","0","64","c7","45","15","3","c8","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","98","cd","15","3","3","e8","0","64","c7","45","15","3","c8","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","98","cd"] # incl bad crc
#rxbuffer = ["28","3","9c","49","0","a","3d","b2","28","83","2","11","39","28","3","9c","49","0","a","3d","b2"] # incl error message
#rxbuffer = ["28","3","9c","49","0","a","3d","b2","28","3","9c","49","0","a","3d","b2","29","3","9c","49","0","a","3c","63","29","3","9c","49","0","a","3c","63","2a","3","9c","49","0","a","3c","50","2a","3","9c","49","0","a","3c","50","2b","3","9c","49","0","a","3d","81","2b","3","9c","49","0","a","3d","81","2c","3","9c","49","0","a","3c","36","2c","3","9c","49","0","a","3c","36","2d","3","9c","49","0","a","3d","e7","2d","3","9c","49","0","a","3d","e7","2e","3","9c","49","0","a","3d","d4","2e","3","9c","49","0","a","3d","d4","2f","3","9c","49","0","a","3c","5","2f","3","9c","49","0","a","3c","5","15","3","1","2c","0","18","86","e1","15","3","30","0","3","0","0","0","80","2","44","aa","35","9","df","24","eb","7c","0","f","68","0","0","f","62","0","0","0","96","0","b4","0","0","0","0","0","65","b3","8","0","0","0","0","0","5c","89","26","0","f0","26","7a","c1","c5","15","3","3","e8","0","64","c7","45","15","3","c8","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","98","cd"]  #id 28

silence = False
lastbyte_read = "0"
running = True
lastseen = 0
# This main loop receives frames and decodes them when the line has been idle for a while
print("\nStarting to decode modbus frames\n")
while running:
    # Blocking loop for receiving frames
    while not silence:
        if not testrun: lastbyte_read = HexToStr(ser.read(1))
        timestamp = time.time()

        #wait until there is a moment of silence on the line....and we got a new request, then go through all the frames in the buffer
        if (timestamp > lastseen + 1) and (len(rxbuffer)>5): # 1 second of silence...
          if debug > 0: 
            print("\nRaw data: ", end='')
            for x in range (0,len(rxbuffer)-1):
                value = rxbuffer[x]
                print(value,",", end='')
            print(" ")

          pointer = 0 # pointer into rxbuff
          temp_buff = list()
          while pointer < len(rxbuffer):
            if rxbuffer[pointer + 1] == "3": #Read 
               temp_buff = rxbuffer[pointer:pointer+8]
               request_number_reg = 256*HexToInt(rxbuffer[pointer+4]) + HexToInt(rxbuffer[pointer+5])
               status = DecodeModbusFrame(temp_buff)
               pointer = pointer + 8
               if status == False: #Meaning crc error etc
                 pointer = len(rxbuffer) + 1
               if (len(rxbuffer) > pointer):
                 len_resp = HexToInt(rxbuffer[pointer+2])
                 # check if it's a response to the request above by matching the length
                 if (rxbuffer[pointer + 1] == "3" and request_number_reg * 2 == len_resp):
                   len_resp += 5
                   temp_buff = rxbuffer[pointer:pointer+len_resp]
                   status = DecodeModbusFrame(temp_buff)
                   pointer = pointer  + len_resp
                   if status == False: #Meaning crc error etc
                     pointer = len(rxbuffer) + 1
            elif rxbuffer[pointer + 1] == "10": #Write multiple, assume request comes first....
               num_registers = 256*HexToInt(rxbuffer[pointer+4]) + HexToInt(rxbuffer[pointer+5])
               num_bytes = HexToInt(rxbuffer[pointer+6])
               temp_buff = rxbuffer[pointer:pointer+9+num_bytes]
               status = DecodeModbusFrame(temp_buff)
               pointer = pointer + 9 + num_bytes
               if status == False: #Meaning crc error etc
                 pointer = len(rxbuffer) + 1
               if len(rxbuffer) > pointer:
                 len_resp = 8
                 temp_buff = rxbuffer[pointer:pointer+len_resp]
                 status = DecodeModbusFrame(temp_buff)
                 pointer = pointer  + len_resp
                 if status == False: #Meaning crc error etc
                   pointer = len(rxbuffer) + 1
            elif (rxbuffer[pointer + 1] == "83" or rxbuffer[pointer + 1] == "86" or rxbuffer[pointer + 1] == "90") : #exceptions...
                 temp_buff = rxbuffer[pointer:pointer+5]
                 status = DecodeModbusFrame(temp_buff)
                 pointer = pointer  + 5
                 if status == False: #Meaning crc error etc
                   pointer = len(rxbuffer) + 1
            else:
              print("Not handling this function code:",rxbuffer[pointer + 1])
              print("Maybe the buffer need some time to get on top of things.\n")
              pointer = len(rxbuffer) + 1
          silence = True
        rxbuffer.append(lastbyte_read)
        lastseen = timestamp

    # Succesfully received frame(s), clear and start over
    rxbuffer = list()
    rxbuffer.append(lastbyte_read) #Append the byte that was received after the silence
    silence = False
    if testrun: running = False
