from pymodbus.client.sync import ModbusSerialClient

# Connect RS485 modbus rtu interface 
#
# 2023 Per Carlén

import struct
import time
import requests
import os
import time
from datetime import datetime

modbus_port = '/dev/ttyACM0'
log_path = '/tmp/'
log_modbus = 'modbus.log'
probe_interval = 1


def append2log(msg):
  filename = log_path + "/" + log_modbus
  with open(filename, 'a') as file:
    file.write(msg)


def print2log(msg):
  return msg


def print_rr(lbl,rr,var_type):
  ret = ""
  ret += print2log(str(lbl) + "(" + var_type + "):")
  for x in rr.registers:
    if var_type == "hex":
      s = x.to_bytes(2,"big").hex()
      ret += print2log(s + " ")
    if var_type == "dec":
      ret += print2log(str(x) + ",")
    if var_type == "str":
      s = x.to_bytes(2,"big")
      ret += print2log(str(chr(s[0])) + str(chr(s[1])) + ",")
  ret += print2log("")
  return ret


def read(reg,count,unit_id,var_type):
  ret = ""
  retry_counter = 1
  while retry_counter > 0:
    rr = client.read_holding_registers(reg,count , unit=unit_id)
    if not rr.isError():
      ret += print_rr(str(reg),rr,var_type)
      retry_counter = 0
    if retry_counter > 0:
      time.sleep(0.5)
    retry_counter -= 1
  if retry_counter == 0:
    ret += str(reg) + ", timeout, for unit_id: " + str(unit_id)
  print (ret)
  append2log(ret + "\n")


client = ModbusSerialClient(
    method='rtu',
    port=modbus_port,
    baudrate=9600,
    timeout=1,
    parity='N',
    stopbits=1,
    bytesize=8
)

if not os.path.exists(log_path):
  os.makedirs(log_path)


if client.connect():
  try:
    while True:
      timestamp = str(int(time.time()))
      now = datetime.now()
      dt_string = now.strftime("%Y-%m-%d %H:%M:%S")
      append2log("\n" + timestamp + " - " + dt_string + "\n")
      addr = 1
      while addr < 254:
        print (dt_string + " - Reading from registers, address: " + str(addr))
        read(40001,1,addr,"hex")
        append2log("------------------------\n")
        time.sleep(probe_interval)
        addr += 1
  except KeyboardInterrupt:
    pass
client.close()
