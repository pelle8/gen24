#!/usr/bin/env python

"""
Shows async messages from Tesla Model 3 Battery and close the Contractor.
History:
15.01.2023 initial Version
decode: 0x132, 0x252, 0x352, 0x20A, 0x332, 0x401, 0x2D2, 0x2B4, 0x292
encode: contractor close 0x221
"""

import os
import can
import time
import asyncio
from typing import List

from can.notifier import MessageRecipient

can.rc['interface'] = "socketcan"
can.rc['channel'] = "vcan0"
can.rc['bitrate'] = 500000



debug = False
show_unkown_can_data = False
cell = [] * 96
values = {}
values_strings = {}
unkownCanData = {}
unkownCanDataCounter = {}
contractorText = ["UNKNOWN0",
                  "OPEN    ",
                  "CLOSING ",
                  "BLOCKED ",
                  "OPENING ",
                  "CLOSED  ",
                  "UNKNOWN6",
                  "WELDED  ",
                  "POS CL  ",
                  "NEG CL  ",
                  "UNKNOWN10",
                  "UNKNOWN11",
                  "UNKNOWN12"]
contactorState = ["SNA       ",
                  "OPEN      ",
                  "PRECHARGE ",
                  "BLOCKED   ",
                  "PULLED_IN ",
                  "OPENING   ",
                  "ECONOMIZED",
                  "WELDED    "]
str_kwh=" KWh"
str_kw=" KW"
str_procend=" %"
str_amps=" A"
str_voltage=" V"
str_minutes=" min"
str_celcius=" C"

def can_message(msg: can.Message) -> None:
    if msg is not None:
        if msg.arbitration_id == 0x132:
            frame0x132(msg)
        elif msg.arbitration_id == 0x252:
            frame0x252(msg)
        elif msg.arbitration_id == 0x352:
            frame0x352(msg)
        elif msg.arbitration_id == 0x20A:
            frame0x20a(msg)
        elif msg.arbitration_id == 0x332:
            frame0x332(msg)
        elif msg.arbitration_id == 0x401:
            frame0x401(msg)
        elif msg.arbitration_id == 0x2D2:
            frame0x2d2(msg)
        elif msg.arbitration_id == 0x2B4:
            frame0x2b4(msg)
        elif msg.arbitration_id == 0x292:
            frame0x292(msg)
        elif msg.arbitration_id == 0x3d2:
            frame0x3d2(msg)
        elif msg.arbitration_id == 0x3f2:
            frame0x3f2(msg)
        else:
            if show_unkown_can_data:
                if msg.arbitration_id in unkownCanDataCounter.keys():
                    if(string_from_can_Message_data(unkownCanData[msg.arbitration_id]) != string_from_can_Message_data(msg.data)):
                        unkownCanDataCounter[msg.arbitration_id]=unkownCanDataCounter[msg.arbitration_id]+1
                else:
                    unkownCanDataCounter[msg.arbitration_id]=1
                unkownCanData[msg.arbitration_id] = msg.data

            if debug and msg.arbitration_id == 0x3F2:
                print(msg)


def frame0x132(msg):
    global debug
    volts = extract_k_bits(msg.data, 0, 16, True) * 0.01
    amps = extract_k_bits(msg.data, 16, 16, True) # * 0.1
    if amps > 32768:
       amps = - (65535 - amps)
    amps = amps * 0.1
    raw_amps = extract_k_bits(msg.data, 32, 16, True) * -0.05
    minutes = extract_k_bits(msg.data, 48, 12, True) * 0.1
    values_and_strings("battery_voltage", volts, str_voltage)
    values_and_strings("battery_amps", amps, str_amps)
    values_and_strings("battery_charge_time_remaining", minutes, str_minutes)
    if minutes == 4095:
        minutes = -1
    if debug:
        print("battery voltage: ", volts, "V amps:", amps, "A Raw Amps:", raw_amps, "A Charge Time Remaining ",
              minutes, "min")


def frame0x252(msg):
    global debug
    regenerative_limit = extract_k_bits(msg.data, 0, 16, True) * 0.01
    discharge_limit = extract_k_bits(msg.data, 16, 16, True) * 0.013
    max_heat_park = extract_k_bits(msg.data, 32, 10, True) * 0.01
    hvac_max_power = extract_k_bits(msg.data, 50, 10, True) * 0.02
    values_and_strings("regenerative_limit", regenerative_limit, str_kwh)
    values_and_strings("discharge_limit", discharge_limit, str_kwh)
    values_and_strings("max_heat_park", max_heat_park, str_kwh)
    values_and_strings("hvac_max_power", hvac_max_power, str_kwh)
    if debug:
        print("Reg Power Limit: ", regenerative_limit, "KW, Discharge Limit:", discharge_limit, "KW max Heat park:",
              max_heat_park, "HVAC max Power ", hvac_max_power, "kW")


def frame0x352(msg):
    global debug
    energy_buffer = extract_k_bits(msg.data, 55, 8, True) * 0.1
    energy_to_charge_complete = extract_k_bits(msg.data, 44, 11, True) * 0.1
    expected_energy_remaining = extract_k_bits(msg.data, 22, 11, True) * 0.1
    full_charge_complete = extract_k_bits(msg.data, 63, 1, True)
    ideal_energy_remaining = extract_k_bits(msg.data, 33, 11, True) * 0.1
    nominal_energy_remaining = extract_k_bits(msg.data, 11, 11, True) * 0.1
    nominal_full_pack_energy = extract_k_bits(msg.data, 0, 11, True) * 0.1

    soc = round(expected_energy_remaining / nominal_full_pack_energy * 100)
    values_and_strings("soc", soc, str_procend)
    values_and_strings("energy_buffer", energy_buffer, str_kwh)
    values_and_strings("energy_to_charge_complete", energy_to_charge_complete, str_kwh)
    values_and_strings("expected_energy_remaining", expected_energy_remaining, str_kwh)
    values_and_strings("full_charge_complete", full_charge_complete, "")
    values_and_strings("ideal_energy_remaining", ideal_energy_remaining, str_kwh)
    values_and_strings("nominal_energy_remaining", nominal_energy_remaining, str_kwh)
    values_and_strings("nominal_full_pack_energy", nominal_full_pack_energy, str_kwh)
    if debug:
        print("SOC: ", soc, "% Energy Buffer:", energy_buffer, "KWh, Energy to Charge Complete: ",
              energy_to_charge_complete, "KWh, Excepted energy remaining: ", expected_energy_remaining,
              " KWh, Full Charge Compelte:", full_charge_complete, " , Ideal energy remaining: ",
              ideal_energy_remaining, "KWh, nominal Energy Remaining: ", nominal_energy_remaining,
              "KWh, nominal Full Pack Energy: ", nominal_full_pack_energy, "KWh")


def frame0x20a(msg):
    global contractorText
    contractor = int(msg.data[1] & 0x0F)
    hvil_status = extract_k_bits(msg.data, 40, 4, True)
    packContNegativeState = extract_k_bits(msg.data, 0, 3, True)
    packContPositiveState = extract_k_bits(msg.data, 3, 3, True)
    packContactorSetState = extract_k_bits(msg.data, 8, 4, True)
    packCtrsClosingAllowed = extract_k_bits(msg.data, 35, 1, True)
    pyroTestInProgress = extract_k_bits(msg.data, 37, 1, True)
    text = ""
    if packCtrsClosingAllowed == 0:
        text = "check High Voltage Connectors or Briges on this connectors! Values: "
    if pyroTestInProgress == 1:
        text = "please wait for Pyro Connection check finished, all HV cables successfull connected and verified!"
    if (debug == False):
        os.system('clear')
        print_info()

    print(text, "Contractor: ", contractorText[contractor], ", NegativState:", contactorState[packContNegativeState],
          ", PositivState: ", contactorState[packContPositiveState], ", setState:",
          contactorState[packContactorSetState], ", close allowed: ", packCtrsClosingAllowed, ", Pyrotest:",
          pyroTestInProgress)


def frame0x332(msg):
    global debug
    mux = (msg.data[0])
    mux = mux & 0x03
    if mux == 1:  # then pick out max / min cell volts
        volts = bytes_to_int_reverse(msg, 0, 2)
        volts >>= 2
        volts = volts & 0xFFF
        max_volts = volts / 500

        volts = bytes_to_int_reverse(msg, 2, 2)
        volts = volts & 0xFFF
        min_volts = volts / 500.0

        volts = (msg.data[4])
        max_vno = 1 + (volts & 0x007F)

        volts = (msg.data[5])
        min_vno = 1 + (volts & 0x007F)
        if debug:
            print("Cell No: ", max_vno, "= max: ", max_volts, "V Cell No:", min_vno, "= min:", min_volts, "V")
    if mux == 0:  # then pick out max / min temperatures
        volts = (msg.data[2])
        max_temp = (volts * 0.5) - 40

        volts = (msg.data[3])
        min_temp = (volts * 0.5) - 40
        if debug:
            print("max: ", max_temp, "C min :", min_temp, "C")


def frame0x401(msg):
    global cell, debug
    mux = bytes_to_int_reverse(msg, 0, 1)  # get mux
    # mux = mux & 0x03;

    volts = msg.data[1]  # status byte must be 0x02A
    if volts == 0x02A:
        volts = bytes_to_int_reverse(msg, 2, 2)
        volts = volts / 10000.0
        cell[1 + mux * 3] = volts
        volts = bytes_to_int_reverse(msg, 4, 2)
        volts = volts / 10000.0
        cell[2 + mux * 3] = volts
        volts = bytes_to_int_reverse(msg, 6, 2)
        volts = volts / 10000.0
        cell[3 + mux * 3] = volts
        if debug:
            print("Cell(", (1 + mux * 3), "):", cell[1 + mux * 3], "V Cell(", (2 + mux * 3), "):",
                  cell[2 + mux * 3], "V Cell(", (3 + mux * 3), "):", cell[3 + mux * 3], "V")


def frame0x2d2(msg):
    global debug
    min_voltage = extract_k_bits(msg.data, 0, 16, True) * 0.01 * 2
    max_discharge_current = extract_k_bits(msg.data, 48, 14, True) * 0.128
    max_charge_current = extract_k_bits(msg.data, 32, 14, True) * 0.1
    max_voltage = extract_k_bits(msg.data, 16, 16, True) * 0.01 * 2
    values_and_strings("min_voltage", min_voltage, str_voltage)
    values_and_strings("max_discharge_current", max_discharge_current, str_kw)
    values_and_strings("max_charge_current", max_charge_current, str_amps)
    values_and_strings("max_voltage", max_voltage, str_voltage)
    if debug:
        print("minVoltage: ", min_voltage, "V, MaxDischargeCurrent:", max_discharge_current,
              "KW Max Charge Current:", max_charge_current, "KW Max Voltage ", max_voltage, "V")


def frame0x2b4(msg):
    global debug
    high_voltage = extract_k_bits(msg.data, 10, 12, True) * 0.146484
    low_voltage = extract_k_bits(msg.data, 0, 10, True) * 0.0390625
    output_current = extract_k_bits(msg.data, 24, 12, True) / 100
    values_and_strings("high_voltage", high_voltage, str_voltage)
    values_and_strings("low_voltage", low_voltage, str_voltage)
    values_and_strings("output_current", output_current, str_amps)
    if debug:
        print("HighVoltage Output Pins: ", high_voltage, "V, Low Voltage:", low_voltage, "V Current Output",
              output_current, "A")

def frame0x3d2(msg):
    global debug
    total_discharge=extract_k_bits(msg.data, 0, 32, True) * 0.001
    total_charge=extract_k_bits(msg.data, 32, 32, True) * 0.001
    values_and_strings("total_discharge", total_discharge, str_kwh)
    values_and_strings("total_charge", total_charge, str_kwh)

def frame0x3f2(msg):
    global debug
    mux=extract_k_bits(msg.data, 0, 4, True)
    if mux==0:
        ac_charger_total=extract_k_bits(msg.data, 8, 32, True) * 0.001
        values_and_strings("ac_charger_total", ac_charger_total, str_kwh)
    elif mux==1:
        dc_charger_total=extract_k_bits(msg.data, 8, 32, True) * 0.001
        values_and_strings("dc_charger_total", dc_charger_total, str_kwh)
    elif mux==2:
        regen_charge_total=extract_k_bits(msg.data, 8, 32, True) * 0.001
        values_and_strings("regen_charge_total", regen_charge_total, str_kwh)
    elif mux==3:
        drive_discharge_total=extract_k_bits(msg.data, 8, 32, True) * 0.001
        values_and_strings("drive_discharge_total", drive_discharge_total, str_kwh)
    elif mux==4:
        discharge_total_module1=extract_k_bits(msg.data, 8, 28, True) * 0.001
        values_and_strings("discharge_total_module1", discharge_total_module1, str_kwh)
        charge_total_module1=extract_k_bits(msg.data, 36, 28, True) * 0.001
        values_and_strings("charge_total_module1", charge_total_module1, str_kwh)
    elif mux==5:
        ac_charge_total_module1=extract_k_bits(msg.data, 8, 28, True) * 0.001
        values_and_strings("ac_charge_total_module1", ac_charge_total_module1, str_kwh)
        dc_charge_total_module1=extract_k_bits(msg.data, 36, 28, True) * 0.001
        values_and_strings("dc_charge_total_module1", dc_charge_total_module1, str_kwh)
    elif mux==6:
        discharge_total_module2=extract_k_bits(msg.data, 8, 28, True) * 0.001
        values_and_strings("discharge_total_module2", discharge_total_module2, str_kwh)
        charge_total_module2=extract_k_bits(msg.data, 36, 28, True) * 0.001
        values_and_strings("charge_total_module2", charge_total_module2, str_kwh)
    elif mux==7:
        ac_charge_totalmodule2=extract_k_bits(msg.data, 8, 28, True) * 0.001
        values_and_strings("ac_charge_totalmodule2", ac_charge_totalmodule2, str_kwh)
        dc_charge_total_module2=extract_k_bits(msg.data, 36, 28, True) * 0.001
        values_and_strings("dc_charge_total_module2", dc_charge_total_module2, str_kwh)
    elif mux==8:
        discharge_total_module3=extract_k_bits(msg.data, 8, 28, True) * 0.001
        values_and_strings("discharge_total_module3", discharge_total_module3, str_kwh)
        charge_total_module3=extract_k_bits(msg.data, 36, 28, True) * 0.001
        values_and_strings("charge_total_module3", charge_total_module3, str_kwh)
    elif mux==9:
        ac_charge_total_module3=extract_k_bits(msg.data, 8, 28, True) * 0.001
        values_and_strings("ac_charge_total_module3", ac_charge_total_module3, str_kwh)
        dc_charge_total_module3=extract_k_bits(msg.data, 36, 28, True) * 0.001
        values_and_strings("dc_charge_total_module3", dc_charge_total_module3, str_kwh)
    elif mux==10:
        discharge_total_module4=extract_k_bits(msg.data, 8, 28, True) * 0.001
        values_and_strings("discharge_total_module4", discharge_total_module4, str_kwh)
        charge_total_module4=extract_k_bits(msg.data, 36, 28, True) * 0.001
        values_and_strings("charge_total_module4", charge_total_module4, str_kwh)
    elif mux==11:
        ac_charge_total_module4=extract_k_bits(msg.data, 8, 28, True) * 0.001
        values_and_strings("ac_charge_total_module4", ac_charge_total_module4, str_kwh)
        dc_charge_total_module4=extract_k_bits(msg.data, 36, 28, True) * 0.001
        values_and_strings("dc_charge_total_module4", dc_charge_total_module4, str_kwh)

def frame0x292(msg):
    global debug
    bat_beginning_of_life = extract_k_bits(msg.data, 40, 10, True) / 10
    soc_max = extract_k_bits(msg.data, 20, 10, True) / 10
    soc_ave = extract_k_bits(msg.data, 30, 10, True) / 10
    soc_vi = extract_k_bits(msg.data, 10, 10, True) / 10
    soc_min = extract_k_bits(msg.data, 0, 10, True) / 10
    bms_bat_temp_pct = extract_k_bits(msg.data, 50, 8, True) * 0.4
    values_and_strings("soc_max", soc_max, str_procend)
    values_and_strings("soc_ave", soc_ave, str_procend)
    values_and_strings("soc_vi", soc_vi, str_procend)
    values_and_strings("soc_min", soc_min, str_procend)
    values_and_strings("bms_bat_temp_pct", bms_bat_temp_pct, str_celcius)
    values_and_strings("bat_beginning_of_life", bat_beginning_of_life, str_kwh)
    if debug:
        print("Battery Beginning of Life: ", bat_beginning_of_life, "kWh SOC Max: ", soc_max, "% SOC average: ",
              soc_ave, "% SOC VI: ", soc_vi, "% SOC min: ", soc_min, "% BMS Battery Temp PCT: ", bms_bat_temp_pct)


def send_frame221_contractor(bus, state):
    global debug
    if state:
        # sendFrame332(bus)
        # msg = can.Message(arbitration_id=0x221, timestamp=time.time(), data=[0x41, 0x11, 0x01, 0x00, 0x00,0x00,0x20,0x96 ],is_extended_id=False)
        msg = can.Message(arbitration_id=0x221, timestamp=time.time(),
                          data=[0x40, 0x41, 0x05, 0x15, 0x00, 0x50, 0x71, 0x7f], is_extended_id=False)

        try:
            bus.send(msg)
            if debug:
                print("Contractor Frame 221 61 was successful sent.")
        except can.CanError:
            if debug:
                print(msg, "Contractor Frame 221 61 can not be sent.")
        msg = can.Message(arbitration_id=0x221, timestamp=time.time(),
                          data=[0x60, 0x55, 0x55, 0x15, 0x54, 0x51, 0xd1, 0xb8], is_extended_id=False)
        try:
            bus.send(msg)
            if debug:
                print("Contractor Frame 221 60 was successful sent.")
        except can.CanError:
            if debug:
                print(msg, "Contractor Frame 221 60 can not be sent.")


def send_frame332(bus):
    global debug
    msg = can.Message(arbitration_id=0x332, data=[0x61, 0x05, 0x55, 0x55, 0x00, 0x00, 0xe0, 0x13], is_extended_id=False)
    try:
        bus.send(msg)
        if debug:
            print("Frame 322 - Part 1 was successful sent.")
    except can.CanError:
        if debug:
            print(msg, "Frame 322 - Part 1 can not be sent.")

    msg = can.Message(arbitration_id=0x332, data=[0x3C, 0x0C, 0x85, 0x80, 0x89, 0x86], is_extended_id=False)
    try:
        bus.send(msg)
        if debug:
            print("Frame 322 - Part 2 was successful sent.")
    except can.CanError:
        if debug:
            print(msg, "Frame 322 - Part 2 can not be sent.")


def bytes_to_int_reverse(msg, pos, byte_len):
    result = 0
    for index in range(pos + byte_len - 1, pos - 1, -1):
        result = result * 256 + int(msg.data[index])
    return result


def bytes_to_int(msg, pos, byte_len):
    result = 0
    for index in range(pos, pos + byte_len):
        result = result * 256 + int(msg.data[index])
    return result


def extract_k_bits(bytes_value, position, bit_quantity, little):
    # convert number into binary first
    binary = ""
    position = position
    if little:
        for index in range(0, len(bytes_value)):
            binary_dummy = bin(bytes_value[len(bytes_value) - index - 1])
            # remove first two characters 0b that bin add in the front
            binary += fill_up_bits_string(binary_dummy[2:])
        end = len(binary) - position
        start = len(binary) - position - bit_quantity
    else:
        for index in range(0, len(bytes_value)):
            binary_dummy = bin(bytes_value[index])
            # remove first two characters 0b that bin add in the front
            binary += fill_up_bits_string(binary_dummy[2:])
        end = position + bit_quantity
        start = position


    k_bit_sub_str = binary[start: end]
    return int(k_bit_sub_str, 2)


def fill_up_bits_string(bits):
    result_bits = ""
    for index in range(len(bits), 8):
        result_bits += "0"
    return result_bits + bits


def fill_up_string(str, counter, cut=-1):
    while len(str) < counter:
        str = str + " "
    if cut>0 and len(str)>counter:
        str=str[0:counter]

    return str

def values_and_strings(key, value,str):
    values[key] = value
    values_strings[key]=str

def string_from_can_Message_data(data):
    data_strings = []
    field_strings = []
    if data is not None:
        for index in range(0, len(data)):
            data_strings.append(f"{data[index]:02x}")
    if data_strings:  # if not empty
        field_strings.append(" ".join(data_strings).ljust(24, " "))
    else:
        field_strings.append(" " * 24)

    if (data is not None) and (data.isalnum()):
        field_strings.append(f"'{data.decode('utf-8', 'replace')}'")
    return field_strings
def can_id_to_str(canid):
    return f"ID: {canid:04x}".rjust(6, " ")

def print_info():
    global cell
    print(
        "Tesla Model 3/Y Python Connector by Instagram: alex4skate\nThanks to bielec, Bryan Inkster, Jack Rickard and Collin Kidder")
    lines = [] * 8
    for line in range(0, 8):
        lines.append(" ")
        for index in range(0, 12):
            cell_value = cell[index + (line) * 12 + 1]
            if cell_value == -1.001:
                cell_value = "?.???"
            if (index + 1 + (line) * 12) < 10:
                part = "[0" + str((index + 1 + (line) * 12)) + "]" + str(cell_value) + "V  ";
            else:
                part = "[" + str((index + 1 + (line) * 12)) + "]" + str(cell_value) + "V  ";
            while len(part) < 13:
                part = part + " "
            lines[line] = lines[line] + part
    for line in range(0, 8):
        print(lines[line])
    value_counter = 0;
    for key in values.keys():
        value_counter+=1
        print(" ", fill_up_string(key, 30), ":", fill_up_string(str(values[key]), 6,True),fill_up_string(values_strings[key], 6), end = '')
        if value_counter >=3:
            value_counter=0
            print (" ");
    print(
        "\n\nPress CTRL+C for stopping\nTake care 400Volts! Request for close contractor of the battery will be sent! - Unkown Can Data:\n")
    if show_unkown_can_data:
        for key in unkownCanData.keys():
            value_counter+=1
            print(" ", fill_up_string(can_id_to_str(key), 5), ":", string_from_can_Message_data(unkownCanData[key]) ,fill_up_string( str(unkownCanDataCounter[key]), 5), end = '')
            if value_counter >=3:
                value_counter=0
                print (" ");
        print(" ");

async def main() -> None:
    """Receives all messages and prints them to the console until Ctrl+C is pressed. Send the Contractor Event to the Battery"""

    # this uses the default configuration (for example from environment variables, or a
    # config file) see https://python-can.readthedocs.io/en/stable/configuration.html

    # initialization the array
    print(
        "Tesla Python Connector by Instagram: alex4skate\nThanks to bielec, Bryan Inkster, Jack Rickard and Collin Kidder")
    for index in range(0, 100):
        cell.append(-1.001)
    next_run = 0
    print("initialisation CAN Bus on Pi")
    with can.Bus() as bus:

        # async initialisation
#        reader = can.AsyncBufferedReader()
#        logger = can.Logger("/tmp/logfile.asc")

        listeners: List[MessageRecipient] = [
            can_message,  # Callback function
#            reader,  # AsyncBufferedReader() listener
#            logger,  # Regular Listener object
        ]
        # Create Notifier with an explicit loop to use for scheduling of callbacks
        loop = asyncio.get_running_loop()
        notifier = can.Notifier(bus, listeners, loop=loop)

        try:
            print(
                "started. \nPress CTRL+C for stopping\nTake care 400Volts! Request for close contractor of the battery will be sent!")
            while True:
                if next_run <= time.time():
                    send_frame221_contractor(bus, True)
                    next_run = time.time() + 0.03  # send every 30ms
                await asyncio.sleep(0.01)
        except KeyboardInterrupt:
            # Wait for last message to arrive
            await reader.get_message()
            global debug
            print("Done!")

            # Clean-up
            notifier.stop()
            pass  # exit normally


if __name__ == "__main__":
    asyncio.run(main())
