#!/usr/bin/env python
'''
Pymodbus Synchronous Client Examples
--------------------------------------------------------------------------

The following is an example of how to use the synchronous modbus client
implementation from pymodbus.

Modified to connect to Fronius Gen24, Per Carlen
'''
from pymodbus.client import ModbusTcpClient as ModbusClient

import logging
import time

def print_rr(lbl,rr):
  print(lbl + ":",end='')
  for x in rr.registers:
    s = x.to_bytes(2,"big").hex()
    print(s + " (" + str(x) + ")",end='')
  print("")

def cm10_read(client,cm10_read_input_register):

    for key in cm10_read_input_register:
      print(key,cm10_read_input_register[key])
      rr = client.read_input_registers( key, cm10_read_input_register[key], 1)
      print("input:", rr.registers )

def cm10_write(client,command,power):
  cmd = 0
  if command == "stop": cmd = 0xcc
  if command == "charge": cmd = 0xaa
  if command == "discharge": cmd = 0xbb
  rr = client.write_registers(13049, 4, 0x01) # mode
  rr = client.write_registers(13050, cmd, 0x01)  
  rr = client.write_registers(13051, power, 0x01)  
  #rr = client.write_registers(13079, 20, 0x01)  



def sungrow():
  client = ModbusClient('192.168.5.20', port=1502)

  cm10_read_input_register = { 4989:10, 5016:2, 5627:1, 12999:1, 13000:1, 13009:2, 13021:1, 13022:1, 13023:1, 13024:1, 13038:1 } 

  while True:

    for x in range(10):
      print("Reading input registers, charge 500")
      cm10_read(client,cm10_read_input_register)
      cm10_write(client,"charge",500)
      time.sleep(1)

    for x in range(10):
      print("Reading input registers, charge 1000")
      cm10_read(client,cm10_read_input_register)
      cm10_write(client,"charge",1000)
      time.sleep(1)

    for x in range(60):
      print("Reading input registers, stop")
      cm10_read(client,cm10_read_input_register)
      cm10_write(client,"stop",0)
      time.sleep(1)

    for x in range(10):
      print("Reading input registers, discharge 500")
      cm10_read(client,cm10_read_input_register)
      cm10_write(client,"discharge",500)
      time.sleep(1)


  client.close()


def sungrow_cm10():
  client = ModbusClient('192.168.5.20', port=1502)
  rr = client.read_input_registers( 13038, 1, 1)
  print("input_13038(cap):", rr.registers )
  rr = client.read_input_registers( 13023, 1, 1)
  print("input_13023(soh):", rr.registers )
  rr = client.read_input_registers( 13022, 1, 1)
  print("input_13022(soc):", rr.registers )
  rr = client.read_input_registers( 13021, 1, 1)
  print("input_13021(bat power):", rr.registers )
  rr = client.read_holding_registers( 13049, 3, 1)
#  print("holding:", rr.registers )
  print("Writing..")
#  rr = client.write_registers(13050, [0xcc,0], 0x01) # stop
#  rr = client.write_registers(13050, [0xbb,1000], 0x01) # discharge
#  rr = client.write_registers(13050, [0xaa,500], 0x01) # charge
  rr = client.write_registers(13049, 4, 0x01) # stop
  rr = client.write_registers(13050, 0xcc, 0x01) # stop
  rr = client.write_registers(13051, 0, 0x01) # stop

  client.close()

def fronius():
  client = ModbusClient('192.168.5.11', port=502)
  client.connect()
#  rr = client.write_registers(40345, 10000, 0x01) # 

  rr = client.read_holding_registers(40345, 1, 0x01)
  print_rr("WChamax",rr)

#  rr = client.write_registers(40350, 1100, 0x01) # soc
#  rr = client.write_registers(40355, 65534, 0x01) # outrte 
##  rr = client.write_registers(40356, 65535, 0x01) # inwrte 
#  rr = client.write_registers(40348, 3, 0x01) # Charge limit
#  rr = client.write_registers(40232, 1000, 0x01) 

# Charge 100W???
#  rr = client.write_registers(40348, 3, 0x01) # Charge limit
#  rr = client.write_registers(40355, 100, 0x01) # outrte (disch)
#  rr = client.write_registers(40356, 65436, 0x01) # inwrte (ch)

  # Discharge 2000W:
  rr = client.write_registers(40348, 0, 0x01) # self optimize
#  rr = client.write_registers(40355, 60536, 0x01) # outrte (disch)
#  rr = client.write_registers(40356, 5000, 0x01) # inwrte (ch)
  rr = client.write_registers(40355, 64000, 0x01) # outrte (disch)
  rr = client.write_registers(40356, 1000, 0x01) # inwrte (ch)

#  rr = client.write_registers(40348, 3, 0x01) # Charge limit
#  rr = client.write_registers(40348, 0, 0x01) # Charge limit
#  rr = client.write_registers(40355, [0,0], 0x01) # inwrte (ch)
#  rr = client.write_registers(40348, 0, 0x01) # Charge limit
#  rr = client.write_registers(40355, [0,0], 0x01) # inwrte (ch)


#  rr = client.write_registers(40348, 3, 0x01) # Charge limit
#  rr = client.write_registers(40355, 2007, 0x01) # Charge limit
#  rr = client.write_registers(40356, 63529, 0x01) # Charge limit



  #rr = client.write_registers(40350, 500, unit=0x01) # soc 5%
#  addr = 40342
#  while addr < 40370:
  addr = 40349
  while addr < 40357:
    rr = client.read_holding_registers( addr, 1, 1)
    try:
      rr.registers
    except:
      print(str(addr) + ":" + str(rr))
    else:
      print_rr(str(addr+1),rr)
    addr += 1

  rr = client.read_holding_registers(40274, 70, 0x01)
  print_rr("P PV1",rr)
  print("aaa",rr.registers[60])
  rr = client.read_holding_registers(40294, 1, 0x01)
  print_rr("P PV2",rr)
  rr = client.read_holding_registers(40354, 1, 0x01)
  print_rr("ChaSt",rr)
  rr = client.read_holding_registers(40083, 2, 0x01)
  print_rr("AC POWER",rr)

  rr = client.read_holding_registers(40257, 1, 0x01)
  print_rr("SCALE",rr)

  rr = client.write_registers(40350, 1400, 0x01) # Charge limit

  rr = client.read_holding_registers(40351, 1, 0x01)
  print_rr("SOC",rr)

#  rr = client.write_registers(40348, 2, 0x01) # Charge limit
#  rr = client.write_registers(40355, 100, 0x01) # outrte 
#  rr = client.write_registers(40356, 0, 0x01) # inwrte 

  #rr = client.write_registers(40348, 1, unit=0x01) # disCharge limit
  #rr = client.write_registers(40355, 0, unit=0x01) # outrte 
  #rr = client.write_registers(40356, 64300, unit=0x01) # inwrte 

  #rr = client.read_holding_registers(40348, 1, unit=0x01)
  #print_rr("StorCtl_Mod",rr)
  #rr = client.read_holding_registers(40354, 1, unit=0x01)
  #print_rr("ChaSt",rr)
  #rr = client.read_holding_registers(40360, 1, unit=0x01)
  #print_rr("CHaGriSet",rr)
  #rr = client.read_holding_registers(40355, 1, unit=0x01)
  #print_rr("OutWRte",rr)
  #rr = client.read_holding_registers(40356, 1, unit=0x01)
  #print_rr("InWRte",rr)
  #rr = client.read_holding_registers(40350, 1, unit=0x01)
  #print_rr("MinRsvPct",rr)
  #rr = client.read_holding_registers(40351, 1, unit=0x01)
  #print_rr("ChaState",rr)
  #rr = client.read_holding_registers(40232, 1, unit=0x01)
  #print_rr("WMaxLimPct",rr)
  #rr = client.read_holding_registers(40107, 1, unit=0x01)
  #print_rr("St",rr)
  client.close()

#fronius()
sungrow()
#pi()

