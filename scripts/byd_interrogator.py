from pymodbus.client.sync import ModbusSerialClient

# Connect RS485 modbus interface to the bus, already connecting a Gen24 and a BYD.
# The script will query the BYD and send API-requests to Gen24. This will be logged for later analysis.
#
# 2023 Per Carlén

import struct
import time
import requests
import os
import time
from datetime import datetime

modbus_port = '/dev/ttySC0'
log_path = '/tmp/byd'
log_modbus = 'modbus.log'
probe_interval = 60
gen24_address = 'http://192.168.2.5'
gen24_urls = ['/solar_api/v1/GetInverterRealtimeData.cgi','/solar_api/v1/GetStorageRealtimeData.cgi','/solar_api/v1/GetPowerFlowRealtimeData.fcgi','/solar_api/v1/GetMeterRealtimeData.cgi','/components/cache/readable']


def append2log(msg):
  filename = log_path + "/" + log_modbus
  with open(filename, 'a') as file:
    file.write(msg)


def print2log(msg):
  return msg


def print_rr(lbl,rr,var_type):
  ret = ""
  ret += print2log(str(lbl) + "(" + var_type + "):")
  for x in rr.registers:
    if var_type == "hex":
      s = x.to_bytes(2,"big").hex()
      ret += print2log(s + " ")
    if var_type == "dec":
      ret += print2log(str(x) + ",")
    if var_type == "str":
      s = x.to_bytes(2,"big")
      ret += print2log(str(chr(s[0])) + str(chr(s[1])) + ",")
  ret += print2log("")
  return ret


def read(reg,count,unit_id,var_type):
  ret = ""
  retry_counter = 5
  while retry_counter > 0:
    rr = client.read_holding_registers(reg,count , unit=unit_id)
    if not rr.isError():
      ret += print_rr(str(reg),rr,var_type)
      retry_counter = 0
    if retry_counter > 0:
      time.sleep(0.5)
    retry_counter -= 1
  if retry_counter == 0:
    ret += str(reg) + ", timeout"
  append2log(ret + "\n")


def read_from_gen24(timestamp):
  for url_part in gen24_urls:
    url = gen24_address + url_part
    #print (url)
    r = requests.get(url)
    #print (r.text)
    log_dir = log_path + "/" + timestamp
    if not os.path.exists(log_dir):
      os.makedirs(log_dir)
    filename = log_dir + "/" + os.path.basename(url)
    print ("Writing to " + filename)
    with open(filename, 'a') as temp_file:
      temp_file.write("Response from " + url)
      temp_file.write(r.text)


client = ModbusSerialClient(
    method='rtu',
    port=modbus_port,
    baudrate=9600,
    timeout=1,
    parity='N',
    stopbits=1,
    bytesize=8
)

if not os.path.exists(log_path):
  os.makedirs(log_path)


if client.connect():
  try:
    while True:
      timestamp = str(int(time.time()))
      now = datetime.now()
      dt_string = now.strftime("%Y-%m-%d %H:%M:%S")
      append2log("\n" + timestamp + " - " + dt_string + "\n")
      print (dt_string + " - Reading from registers and Gen24")
      read(100,2,0x15,"hex")
      read(102,16,0x15,"str")
      read(118,26,0x15,"str")
      read(134,19,0x15,"str")
      read(150,18,0x15,"hex")
      read(166,2,0x15,"hex")
      read(200,13,0x15,"dec")
      read(300,24,0x15,"dec")
      read(400,20,0x15,"hex")
      if not os.path.exists(log_path + "/" + timestamp):
        os.makedirs(log_path + "/" + timestamp)
      read_from_gen24(timestamp)
#      read(1000,25,0x15)
#      read(1025,25,0x15)
#      read(1050,25,0x15)
#      read(1075,25,0x15)
      append2log("------------------------\n")
      time.sleep(probe_interval)
  except KeyboardInterrupt:
    pass
client.close()
