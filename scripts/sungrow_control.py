#!/usr/bin/env python
'''
Controls Sungrow (SH10RT) inverter over modubs tcp, Per Carlen
'''
from pymodbus.client import ModbusTcpClient as ModbusClient
import logging
import argparse

def print_rr(lbl,rr):
  print(lbl + ":",end='')
  try:
    for x in rr.registers:
      s = x.to_bytes(2,"big").hex()
      print(s + " (" + str(x) + ")",end='')
  except:
    pass
  print("")

def run() -> None:
  parser = argparse.ArgumentParser("sungrow_control.py")
  parser.add_argument("ip", help="IP address to sungrow inverter listening on tcp modbus", type=str)
  parser.add_argument("command", help="charge/discharge/stop", type=str)
  parser.add_argument("power", help="the power in W", type=int)
  args = parser.parse_args()
  client = ModbusClient(args.ip, port=502)
  client.connect()
  
  # EMS mode 2 - Forced mode 
  ems_mode = 2
  if args.command == "charge": ems_command = 0xaa
  if args.command == "discharge": ems_command = 0xbb
  if args.command == "stop": 
    ems_command = 0xcc
    ems_mode = 0

  rr = client.write_registers(13049, ems_mode, slave=0x01)

  rr = client.write_registers(13050, ems_command, slave=0x01)   #aa bb cc charge/disch/stop
  rr = client.write_registers(13051, args.power, slave=0x01)   #aa bb cc charge/disch/stop

  # charge 300W
#  rr = client.write_registers(13050, 0xaa, slave=0x01)   #aa bb cc charge/disch/stop
#  rr = client.write_registers(13051, 300, slave=0x01)   #aa bb cc charge/disch/stop
  # discharge 200W
#  rr = client.write_registers(13050, 0xbb, slave=0x01)   #aa bb cc charge/disch/stop
#  rr = client.write_registers(13051, 200, slave=0x01)   #aa bb cc charge/disch/stop

  rr = client.read_holding_registers(13049, 1, slave=0x01)
  print_rr("ems mode selection",rr)
  rr = client.read_holding_registers(13050, 1, slave=0x01)
  print_rr("command (charge/discharge/stop",rr)
  rr = client.read_holding_registers(13051, 1, slave=0x01)
  print_rr("power",rr)
  client.close()

if __name__ == "__main__":
    run()
