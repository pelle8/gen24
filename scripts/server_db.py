#!/usr/bin/env python
"""
Pymodbus Server With Updating Thread
- modified to spoof a BYD to GEN24 // Per Carlén
"""
# --------------------------------------------------------------------------- #
# import the modbus libraries we need
# --------------------------------------------------------------------------- #
from pymodbus.server.asynchronous import StartSerialServer
from pymodbus.device import ModbusDeviceIdentification
from pymodbus.datastore import ModbusSlaveContext, ModbusServerContext
from pymodbus.transaction import ModbusRtuFramer, ModbusAsciiFramer
from pymodbus.datastore import ModbusSequentialDataBlock, ModbusSparseDataBlock

from twisted.internet.task import LoopingCall
import sqlite3
from sqlite3 import Error
import time


# --------------------------------------------------------------------------- #
# some global vars
# --------------------------------------------------------------------------- #
timeout_can = 60 		# Go to inactive if db hasn't been updated
hysteresis = 0			# Hysteresis in play (when U correlated to SoC, a battery will recover when not discharge/charge)
debug = 0

# --------------------------------------------------------------------------- #
# configure logging
# --------------------------------------------------------------------------- #
import logging
if debug:
    logging.basicConfig()
    log = logging.getLogger()
    log.setLevel(logging.DEBUG)

# --------------------------------------------------------------------------- #
# connect to sqlite db
# --------------------------------------------------------------------------- #
def create_connection(db_file):
    if debug:
        print("SQL - create_connection - start")
    conn = None
    try:
        conn = sqlite3.connect(db_file)
    except Error as e:
        print(e)
        return
    if debug:
        print("SQL - create_connection - end")
    return conn

# --------------------------------------------------------------------------- #
# select all from table in bms db
# --------------------------------------------------------------------------- #
def select_all_data(conn):
    if debug:
        print("SQL - select_all_data - start")
    cur = conn.cursor()
    try:
        cur.execute("SELECT * FROM BMSDATA")
    except Error as e:
        print(e)
        return
    rows = cur.fetchall()
    if debug:
        print("SQL - select_all_data - end")
    return rows

# --------------------------------------------------------------------------- #
# thread to update the registers from the database
# --------------------------------------------------------------------------- #
def updating_writer(a):
    global hysteresis
    print("")
    if debug:
        print("Updating_writer - start")
    context = a[0]
    register = 3
    slave_id = 0x00
    address = 300
    # Read the current stored values in the registers
    values_200 = context[slave_id].getValues(4, 200, count=13)
    values_300 = context[slave_id].getValues(register, address, count=24)
    values_400 = context[slave_id].getValues(4, 400, count=2)

    # Read from the db
    database = r"/opt/gen24/bms.db"
    conn = create_connection(database)
    if conn is None:
        return
    rows = select_all_data(conn)
    if rows is None:
        return
    conn.close()
    timestamp_now = time.time()

    tuple = rows[0]
    cell_min_u = tuple[1]
    cell_max_u = tuple[2]
    soc_bms = tuple[16]
    soh = tuple[17]
    batt_u = tuple[13]
    batt_i = tuple[14]
    ch_u = tuple[20]
    ch_i = tuple[21]
    charge_p = int(ch_u * ch_i)
    dch_u = tuple[22]
    dch_i = tuple[23]
    discharge_p = int(dch_u * dch_i)
    timestamp_lastupdate = tuple[26]
    target_cell_max_u = tuple[27]
    target_cell_min_u = tuple[28]
    soc_max = tuple[29]
    soc_min = tuple[30]
    mode = tuple[32]
    unknown = tuple[33]
    remain_cap = tuple[18]
    nominal_cap = tuple[19]
    ok2charge = tuple[24]
    ok2discharge = tuple[25]

    status = 4 # 4=FAULT
    if ok2charge == 1 or ok2discharge == 1:
        status = 3 # 3=ACTIVE
    # if db hasn't been updated for a certain time....disable the battery
    if (timestamp_now - timestamp_lastupdate > timeout_can): 
        status = 4 # FAULT 

    # SoC handling, before we got a BMS that is fully aware of the actual SoC,
    # correlate the value with Ubatt. We will then need some hysteresis, since
    # a battery will recover after charging making Ubatt grow and possibly start
    # recharging again...

    if hysteresis == 1:
        print("SoC reached, hysteresis in play")
    soc_old = int(values_300[3]/100)
    soc_ballplank = int( 100* ( (batt_u - values_200[6]/10)) / (values_200[5]/10 - values_200[6]/10))
    print("Last db update:" + str(int(timestamp_now-timestamp_lastupdate)) + " secs ago, Status/Mode:" + str(status) + "/" + str(mode) + ",Ubatt:" + str(batt_u) + ",Umax:" + str(values_200[5]/10) + ",Umin:" + str(values_200[6]/10) + ",SoC:" + str(soc_ballplank) + ", old:" + str(soc_old))
    print("Cell Umax:" + str(cell_max_u) + "(" + str(target_cell_max_u) + "), Cell Umin:" + str(cell_min_u) + "(" + str(target_cell_min_u) + ")")

    if cell_max_u > target_cell_max_u:
       soc_ballplank = soc_max + 1

    if cell_min_u < target_cell_min_u:
       soc_ballplank = soc_min - 1

    soc = soc_ballplank
    if soc_ballplank > soc_max - 1:
       soc = soc_max + 1
       hysteresis = 1
    if soc_ballplank < soc_min + 1:
       soc = soc_min - 1
       hysteresis = 1
    if soc_old > soc_max and hysteresis == 1:
       soc = soc_old
       if soc_ballplank < soc_max - 5:
           soc = soc_ballplank
           hysteresis = 0
    elif soc_old < soc_min and hysteresis == 1:
       soc = soc_old
       if soc_ballplank > soc_min + 5:
           soc = soc_ballplank
           hysteresis = 0

    print("SoC:" + str(soc) + ", Max:" + str(soc_max) + ", Min:" + str(soc_min))

    # Compute the current Power
    r310 = 0
    if tuple[14] <= 0:
        r310 = abs( int(tuple[13] * tuple[14]) )
    if tuple[14] > 0:
        r310 = 65536 - ( int(tuple[13] * tuple[14]) )

    values_300[0] = status             			#301 - status.... 3 = ACTIVE, 4 = INACTIVE
    values_300[1] = 0
    values_300[2] = mode
    values_300[3] = soc * 100      			#304 - soc
    values_300[4] = int(soh/100 * nominal_cap)      	#305 - tot cap
    values_300[5] = int(remain_cap)      		#306 - remaining cap
    values_300[6] = int(discharge_p) 			#307 - max disch power
    values_300[7] = int(charge_p) 			#308 - max charge power
    values_300[8] = int(tuple[13] * 10)      		#309 - Batt Voltage to API
    values_300[9] = r310				#310 - Current Power to API
    values_300[10] = int(tuple[13] * 10)      		#311 - Batt Voltage
    values_300[12] = tuple[6] * 10      		#313 - temp min
    values_300[13] = tuple[7] * 10     			#314 - temp max
    print("200:" + "," . join(str(v) for v in values_200) )
    print("300:" + "," . join(str(v) for v in values_300) )
    print("400:" + "," . join(str(v) for v in values_400) )
    context[slave_id].setValues(register, address, values_300)
    if debug:
        print("Updating_writer - end")
        print("")

def Str2Int32(s):
    a1 = ord(s[0:1])
    if len(s) == 2:
        a2 = ord(s[1:2])
    else:
        a2 = 0
    ret = a1*256 + a2
    return ret

def init_datastore():
    # ----------------------------------------------------------------------- # 
    # initialize your data store
    # ----------------------------------------------------------------------- # 

    database = r"/opt/gen24/bms.db"
    conn = create_connection(database)
    rows = select_all_data(conn)
    conn.close()
    tuple = rows[0]
    nominal_capacity = int(tuple[19])
    ch_u = tuple[20]
    ch_i = tuple[21]
    ch_p = int(ch_u * ch_i)
    cell_max_u = tuple[27]
    cell_min_u = tuple[28]
    num_cells = tuple[31]

    # A Leaf has 48 cells
    batt_max_u = cell_max_u * num_cells
    batt_min_u = cell_min_u * num_cells

    p101 = [ Str2Int32("SI") , 1 ]
    p103 = [ Str2Int32("BY"), Str2Int32("D"), 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
    p119 = [ Str2Int32("BY"), Str2Int32("D "), Str2Int32("Ba"), Str2Int32("tt"), Str2Int32("er"), Str2Int32("y-"), Str2Int32("Bo"), Str2Int32("x "), Str2Int32("Pr"), Str2Int32("em"), Str2Int32("iu"), Str2Int32("m "), Str2Int32("HV"), 0, 0, 0]
    p135 = [ Str2Int32("5."), Str2Int32("0"), 0, 0, 0, 0, 0, 0, Str2Int32("3."), Str2Int32("16"), 0, 0, 0, 0, 0, 0]
    p151 = [ Str2Int32("Pe"), Str2Int32("ll"), Str2Int32("e"), 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 ]
    p167 = [ 1, 0 ]
    p201 = [0, 0, nominal_capacity, nominal_capacity, ch_p, int(batt_max_u*10), int(batt_min_u*10), 133, 10, 133, 10, 0, 0]
#    p301 = [1, 0, 16, 7500, 10948, 514, 0, 8352, 0, 0, 2058, 0, 60, 70, 200, 300, 16, 22741, 400, 500, 13, 52064, 80, 9900]
    p301 = [1, 0, 16, 7500, 10948, 514, 0, 8352, 0, 0, 2058, 0, 60, 70, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]

    block = ModbusSparseDataBlock({ 101: p101, 103:p103, 119:p119, 135:p135, 151:p151, 167:p167, 201:p201, 301:p301, 401:[0]*20, 1001:[0]*100, 12289:[0]*768 })
    store = ModbusSlaveContext(di=block, co=block, hr=block, ir=block,unit=21)
    context = ModbusServerContext(slaves= store, single=True)
    return context

def run_updating_server():

    if debug:
        log.debug("init datastore")
    context = init_datastore()
    tmp=(context,)
    updating_writer(tmp)
    # ----------------------------------------------------------------------- # 
    # initialize the server information
    # ----------------------------------------------------------------------- # 
    identity = ModbusDeviceIdentification()
    identity.VendorName = 'pymodbus'
    identity.ProductCode = 'PM'
    identity.VendorUrl = 'http://github.com/riptideio/pymodbus/'
    identity.ProductName = 'pymodbus Server'
    identity.ModelName = 'pymodbus Server'

    # ----------------------------------------------------------------------- # 
    # run the server you want
    # ----------------------------------------------------------------------- # 
    time_loop = 5  # 5 seconds delay
    loop = LoopingCall(f=updating_writer, a=(context,))
    loop.start(time_loop, now=False) # initially delay by time

    # wait until values are inserted from db
    time.sleep(10)
    StartSerialServer(context, framer=ModbusRtuFramer, identity=identity,port='/dev/ttyUSB0', timeout=.005, baudrate=9600)


if __name__ == '__main__':
    run_updating_server()
