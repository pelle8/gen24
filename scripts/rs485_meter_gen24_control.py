import time
import serial
import re
import sys
import json

from pymodbus.client.sync import ModbusTcpClient as ModbusClient
from datetime import datetime

line = ""
reg_time = [0,0,0]
reg_181 = [0,0,0]
reg_281 = [0,0,0]
list_p_consume = [0,0,0]
list_p_produce = [0,0,0]
wchamax = 0


def write_json(consumed,produced,power_now_grid,inverter_power):
  date_now = datetime.today().strftime('%Y%m%d')

  energy_data = { 'date_now':date_now, 'e_tot_produced': produced, 'e_tot_consumed': consumed, 'power_now_grid': power_now_grid , 'inverter_power': inverter_power} 
  print("Json:",energy_data)
  f = open("/var/www/html/gridpoint.json", "w")
  f.write(json.dumps(energy_data))
  f.close()
  f = open("/tmp/energy.log", "a")
  f.write(json.dumps(energy_data)+"\n")
  f.close()



def shift(reg,v):

  i = 1
  while i < len(reg):
    reg[i-1] = reg[i]
    i += 1
  reg[len(reg)-1] = v
  return reg

def check_line(l):
  global lasttime
  global reg_time
  global reg_181
  global reg_281
  global list_p_consume
  global list_p_produce


#  if re.search("^F\.F",l): # Start of record

  if re.search("^0\.9\.1",l): # Time in format HHMMSS
    t = re.findall(r'\(([^\]]*)\)', l)[0]
    seconds = int(t[0:2]) * 3600 + int(t[2:4]) * 60 + int(t[4:6])
    reg_time = shift(reg_time,seconds)
    #print("Time: ",seconds)

  if re.search("^1\.8\.1",l): # Consumed
    v = re.findall(r'\(([^\]]*)\*', l)[0]
    e = float(v)
    reg_181 = shift(reg_181,e)
    #print("1.8.1: ",e)

  if re.search("^2\.8\.1",l): # Produced
    v = re.findall(r'\(([^\]]*)\*', l)[0]
    e = float(v)
    reg_281 = shift(reg_281,e)
    #print("2.8.1: ",e)

  if re.search("^4\.8\.1",l): # Last one...lets calculate current power
    if reg_181[len(reg_181)-1] > 0 and reg_181[len(reg_181)-2] > 0: # only if we got at least two values....
      if reg_281[len(reg_281)-1] > 0 and reg_281[len(reg_281)-2] > 0: # only if we got at least two values....
        time_last = reg_time[len(reg_181)-1]
        time_prev = reg_time[len(reg_181)-2]
        timediff = time_last - time_prev
        if timediff < 0: # New day
          timediff = time_last + (86400 - time_prev)
        if timediff > 0:
          p_consume = int(3600 * 1000 * ( reg_181[len(reg_181)-1] - reg_181[len(reg_181)-2] ) / timediff)
          p_produce = int(3600 * 1000 * ( reg_281[len(reg_281)-1] - reg_281[len(reg_281)-2] ) / timediff)
          print("P consume:",str(p_consume))
          print("P produce:",str(p_produce))
          list_p_consume = shift(list_p_consume,p_consume)
          list_p_produce = shift(list_p_produce,p_produce)


def adjust_inverter_power(p):

  global client

  # Negative values means consuming from grid
  print("Will adjust to:",str(p))

  if p == -99999: # Out of time control
    print("resetting registers")
    rr = client.write_registers(40348, 0, unit=0x01) # limit
    rr = client.write_registers(40355, 0, unit=0x01) # outwrte 
    rr = client.write_registers(40356, 0, unit=0x01) # inwrte 
    return

  rr = client.read_holding_registers(40355, 1, unit=0x01)
  try:
    outwrte = rr.registers[0]
  except:
    print("Couldn't read from tcp modbus")
    return 0

  rr = client.read_holding_registers(40356, 1, unit=0x01)
  try:
    inwrte = rr.registers[0]
  except:
    print("Couldn't read from tcp modbus")
    return 0

  ret = 0
  err = 0

  if err == 0:
    print(" current inwrte:",inwrte)
    print(" current outwrte:",outwrte)
    if inwrte == 0:
      inwrte = 65535
    if outwrte == 0:
      outwrte = 65535
    print(" new inwrte:",inwrte)
    print(" new outwrte:",outwrte)

    if (p<0 and outwrte > 65530) or (p>=0 and inwrte < 65531): # discharge battery
      value = -p / wchamax * 10000
      reg = 65535 - int(value)
      print(" calc reg:",reg)
      diff = 65535 - reg
      print(" diff:",diff)
      if diff > 0:
        diff = int(diff*0.5) # Don't do everything at once... if increasing the power
      reg = inwrte - diff
      if reg > 65535:
        reg = 65535
      if reg < 32768:
        reg = 32768
      print(" new reg:",reg)
      ret = -(65535 - reg) * wchamax / 10000
      rr = client.write_registers(40348, 1, unit=0x01) # disCharge limit
      rr = client.write_registers(40355, 0, unit=0x01) # outwrte 
      rr = client.write_registers(40356, reg, unit=0x01) # inwrte 

    if (p>0 and inwrte > 65530) or (p<=0 and outwrte < 65531): # charge battery
      value = p / wchamax * 10000
      reg = 65535 - int(value)
      print(" calc reg:",reg)
      diff = 65535 - reg
      print(" diff:",diff)
      if diff > 0:
        diff = int(diff*0.75) # Don't do everything at once... if increasing the power
      reg = outwrte - diff
      if reg > 65535:
        reg = 65535
      if reg < 32768:
        reg = 32768
      print(" new reg:",reg)
      #reg = 65534
      ret = (65535 - reg) * wchamax / 10000
      rr = client.write_registers(40348, 2, unit=0x01) # Charge limit
      rr = client.write_registers(40355, reg, unit=0x01) # outwrte 
      rr = client.write_registers(40356, 0, unit=0x01) # inwrte 

    if (p == 0 and inwrte > 65530 and outwrte > 65530): # reset all
      rr = client.write_registers(40348, 0, unit=0x01) # limit
      rr = client.write_registers(40355, 0, unit=0x01) # outwrte 
      rr = client.write_registers(40356, 0, unit=0x01) # inwrte 
  return ret


def check_power():

  ret = 0

  cons_tot = 0
  prod_tot = 0

  i = 2
  while i < len(list_p_produce):
    cons_tot += list_p_consume[i]
    prod_tot += list_p_produce[i]
    i += 1
  print(list_p_consume)
  print(list_p_produce)
  cons_avg = int(cons_tot / 1)
  prod_avg = int(prod_tot / 1)
  print("Currently producing in time span, avg:",str(prod_avg))
  print("Currently consuming in time span, avg:",str(cons_avg))
  ret = -cons_avg
  ret = prod_avg - cons_avg
  return ret

def print_char(b):
  global line

  ch = ord(b.decode("ascii"))
  if (ch>31 and ch<128):
    line += b.decode("ascii")
    #print(b.decode("ascii"), end = '')
  if ch==2:
    line += "<STX>"
  if ch==3:
    line += "<ETX>"
  if ch==4:
    line += "<EOT>"
  if ch==13:
    print(line)
    check_line(line)
    line = ""

def print_rr(lbl,rr):
  print(lbl + ":",end='')
  for x in rr.registers:
    s = x.to_bytes(2,"big").hex()
    print(s + " (" + str(x) + ")",end='')
  print("")



client = ModbusClient('192.168.2.109', port=502)
client.connect()
rr = client.read_holding_registers(40345, 1, unit=0x01)
try:
  wchamax = rr.registers[0]
except:
  print("Couldn't read tcp modbus")
  exit(1)



print("WChamax:",wchamax)


if len(sys.argv) > 1:
  if sys.argv[1] == "check_line":
    lines = sys.argv[2].split('\n')
    for l in lines:
      check_line(l)
    p = check_power()
    print("Current P:",str(p))
    adjust_inverter_power(p)
    exit(0)


# configure the serial connections (the parameters differs on the device you are connecting to)
ser = serial.Serial(
#    port='/dev/ttyACM0',
    port='/dev/ttySC0',
    baudrate=9600,
    parity=serial.PARITY_EVEN,
    stopbits=serial.STOPBITS_ONE,
    bytesize=serial.SEVENBITS
)

z = 0
ser.isOpen()
time.sleep(1)

while 1 :

    # empty serial in-buffer
    while ser.inWaiting() > 0:
      out = ser.read(1)
      print_char(out)
      time.sleep(0.005)

    ident_rec = 0
    while ident_rec < 2:
      if z == 0:
        s = "\x2f\x31\x21\x0d\x0a"
        b = bytes(s, 'ascii')
        print("Sending request:" + str(b))
        ser.write(b)
        time.sleep(2)
        ok = 0
        while ser.inWaiting() > 0:
            out = ser.read(1)
            print_char(out)
            if re.search("\/",line): # Got identification 
              ident_rec = 1
            if re.search("STX",line) and ident_rec == 1: # Got identification 
              ident_rec = 2
            time.sleep(0.005)
      if ident_rec < 2:
        time.sleep(1)

    if reg_181[len(reg_181)-1] > 0 and reg_281[len(reg_281)-1] > 0:
      p_now = check_power()
      p = p_now
      print("Current P:",str(p_now))
      t = reg_time[len(reg_time)-1]
      print("t:",t)
      if ((t > 3600*20) and (t < 86401)) or ((t >= 0) and (t < 3600*5)):
        p = -99999
        print("Time is up, letting Gui do the rest")
      p_inv = adjust_inverter_power(p)
      write_json(reg_181[len(reg_181)-1],reg_281[len(reg_281)-1],p_now,p_inv)


    time.sleep(1)

    if z == 0:
      s = "\x06\x30\x35\x30\x0d\x0a"
      b = bytes(s, 'ascii')
      print("Sending ACK:" + str(b))
      ser.write(b)
      time.sleep(2)
      while ser.inWaiting() > 0:
          out = ser.read(1)
          print_char(out)
          time.sleep(0.005)

    print("sleep 1")
    time.sleep(5)

    # empty serial in-buffer
    while ser.inWaiting() > 0:
      out = ser.read(1)
      print_char(out)
      time.sleep(0.005)

    print("sleep 2")
    time.sleep(5)

    # empty serial in-buffer
    while ser.inWaiting() > 0:
      out = ser.read(1)
      print_char(out)
      time.sleep(0.005)

    print("sleep 3")
    time.sleep(20)
