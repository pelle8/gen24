#!/usr/bin/env python

# 2023 Per Carlen
#
# This is a decoder for Juntek BMS
#
# Inspiration from: https://github.com/opless/juntek-vat-python
#

import time
import serial
import json
from datetime import datetime


def decode(ba) -> None:
    # Let's compute crc (last byte)...
    crc = 0
    for idx, b in enumerate(ba):
      if idx < len(ba) - 1: crc += b
    crc = crc & 255
    json_string = ""
    if crc == ba[len(ba)-1]:
      time_now = time.time()
      voltage = (ba[4] * 256 + ba[5]) / 100
      current = (ba[6] * 256 + ba[7]) 
      if current > 32767:
        current = - (current ^ 65535) + 1
      current = current / 10
      power = (ba[8] * 256 *256 *256  + ba[9]* 256 *256 +  ba[10]* 256 + ba[11]) / 1000
      amp_hours = (ba[12] * 256 *256 *256  + ba[13]* 256 *256 +  ba[14]* 256 + ba[15]) / 1000
      temp = ba[25] / 10
      if temp > 127:
        temp = - (temp ^ 65535) + 1
      if current < 0 and power > 0:
        power = -power
      json_string = json.dumps({'last_update' : time_now, 'stat_batt_voltage': voltage, 'stat_batt_current': current, 'stat_batt_power': power , 'stat_temp': int(temp),'amp_hours': amp_hours }, separators=(',', ':'))
    else:
      print("ERROR: crc not ok")
    return json_string



# configure the serial connections (the parameters differs on the device you are connecting to)
ser = serial.Serial(
    port='/dev/ttyACM0',
#    port='/dev/ttySC1',
    baudrate=57600,
    parity=serial.PARITY_NONE,
    stopbits=serial.STOPBITS_ONE,
    bytesize=serial.EIGHTBITS
)

ser.isOpen()
time.sleep(1)

batt_amp_hours = 120

while 1 :
    print("")
    # empty serial in-buffer
    while ser.inWaiting() > 0:
      out = ser.read(1)
      time.sleep(0.001)

    s = b'\xaa\x04\x01\x01\xb0'
    date_now = datetime.today() #.strftime('%Y%m%d %h:%M:%s')
    print("Sending request",date_now)
    ser.write(s)

    packet_done = False
    i = 0
    ba = []
    prev = 0
    while not packet_done:
      out = ser.read(1)
      ba.append(ord(out))
      temp = prev * 256 + ord(out)
      prev = ord(out)
      print(i,":",ord(out),"-",temp)
      i = i + 1
      if i > 28:
        packet_done = True
      time.sleep(0.005)
    print(ba)
    json_string = decode(ba)
    print("Decoded as:",json_string)
    time.sleep(5)
