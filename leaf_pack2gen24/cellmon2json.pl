#!/usr/bin/perl

use DBI;
use LWP::Simple qw/get/;
use JSON::XS;
use Data::Dumper::Simple;
use DateTime;

my $dbh;
my $driver   = "SQLite"; 
my $can_database = "/opt/gen24/db/batrium.db";
my $dsn_can = "DBI:$driver:dbname=$can_database";
my $userid = "";
my $password = "";

sub str2int16 {
	my $s = shift;
	$value = hex($s);
	print "v:$value, hex:$s\n" if $debug > 1;
	$value -= 0x10000 if $value >= 0x8000;
	return $value;
}

sub str2uint16 {
	my $s = shift;
	$value = hex($s);
	return $value;
}

sub str2uint32 {
	my $s = shift;
	$value = hex($s);
	return $value;
}

sub str2float {
	my $s = shift;
	$value = unpack "f",reverse pack "H*", $s;
#	$value = unpack "f", pack "H*", $s;
	return $value;
}

sub DecodeCANmessage {
	my $id = shift;
	my $data = shift;

	my $i = 0;
        my @bytes;
	while ($i < length($data)) {
          $h = substr($data,$i*2,2);
	  push @bytes,$h;
          $i++;
        }
	
        my $p = 1;
	print "ID:$id,data:$data\n" if $debug > 0;
	my $statement = "";
	my $ret = "";

   $hit = 0;

   if ($id =~ /1d11/) {
        $hit = 1;
#	$ret = " Cellmon stats\n";
	$mincellvoltage = str2uint16($bytes[1] .$bytes[0]) / 1000;
	$maxcellvoltage = str2uint16($bytes[3] .$bytes[2]) / 1000;
        $cell_temp = hex($bytes[4]) - 40;
        $bypass_temp = hex($bytes[5]) - 40;
        $bypass_pwm = hex($bytes[6]);
#	$ret .= "  MinCell U: $mincellvoltage, MaxCell U: $maxcellvoltage, CellTemp: $cell_temp, BypassTemp: $bypass_temp, BypassPWM: $bypass_pwm";
	$ret .= "$mincellvoltage;$maxcellvoltage;$cell_temp;$bypass_temp;$bypass_pwm";
   }


	return $ret;
}


my $timestamp = time();
$debug = 0;

my %can_registers;
my $j = 0;

while (1) {

  my $timestamp_now = time();
  print "\n\nTimestamp:$timestamp_now\n";
  $dbh = DBI->connect($dsn_can, $userid, $password, { RaiseError => 1,sqlite_open_flags => DBD::SQLite::OPEN_READONLY }) or die $DBI::errstr;
  $statement = "SELECT * FROM BATRIUM WHERE ID LIKE '%1d11__'";
  $sth = $dbh->prepare($statement) or warn "error";
  $sth->execute() or warn "error";
  my $cr = "\n";
  open(FH, '>', '/var/www/html/data/cellstats.json') or warn $!;
  print FH '{ ' . $cr;
  my($id,$lastupdate,$data);
  print FH ' "Cells" : { ' . $cr;
  my $begin = 1;
  my $last_timestamp = 0;
  while(($id,$lastupdate,$data) = $sth->fetchrow()) {
	print FH ', ' . $cr if $begin == 0;
	$begin = 0;
	my $timestamp_now = time();
    	# print "R:$id, D:$data, L:$lastupdate($timestamp_now)\n";
    	$decoded = DecodeCANmessage( $id, $data );
        my $reg = substr($id,4,2);
	my $reg_int = hex($reg);
	my @items = split /;/,$decoded;
    	print "$lastupdate - Reg:$reg_int, Data: $data - $decoded                             \n";
	$last_timestamp = $lastupdate;
        print FH ' "' . $reg_int . '" : {' . $cr;
        print FH '   "U_CellMin" : "' . $items[0] . '", ' . $cr;
        print FH '   "U_CellMax" : "' . $items[1] . '", ' . $cr;
        print FH '   "t_Cell" : "' . $items[2] . '", ' . $cr;
        print FH '   "t_Bypass" : "' . $items[3] . '", ' . $cr;
        print FH '   "pwm_Bypass" : "' . $items[4] . '" ' . $cr;
  	print FH ' }';
  }
  $sth->finish();
  $dbh->disconnect();
  print FH $cr . ' }, ' . $cr;
  print FH ' "LastUpdate" : "' . int($last_timestamp) . '"' . $cr;
  print FH '} ' . $cr;
  close(FH);
  sleep 5;
}

