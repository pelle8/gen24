# Work In Progress

The scripts in this folder are forming my setup of an old Leaf batterypack dressed up as a BYD.


The components...
- gets CAN-data from a Batrium BMS and puts them in a db
- gets data from a db and transforms them into a new db, for Gen24 purposes
- reads data and from a db and transforms into modbus registers that will be read by a Gen24
- reads data from databases and creates dynamic content on a webserver

## Current status
Messy. But working. 


## Web 
This is what my batterypack looks like, when balancing...

![Gen24-Battery Data](./batt_balancing.png "Battery")

