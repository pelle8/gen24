#!/usr/bin/perl

use DBI;
use LWP::Simple qw/get/;
use JSON::XS;
use Data::Dumper::Simple;
use DateTime;

my $api_url = "http://192.168.2.5/solar_api/v1/GetInverterRealtimeData.cgi";
my $cache_url = "http://192.168.2.5/components/cache/readable";

my $dbh;
my $driver   = "SQLite"; 
my $bms_database = "/opt/gen24/db/bms.db";
my $can_database = "/opt/gen24/db/batrium.db";
my $dsn_bms = "DBI:$driver:dbname=$bms_database";
my $dsn_can = "DBI:$driver:dbname=$can_database";
my $userid = "";
my $password = "";

my $counter = 0;

my $target_cell_max_u = 4.2;
my $target_cell_min_u = 3.5;
my $target_soc_max = 100;
my $target_soc_min = 19;
my $nominal_capacity = int(24000);
my $num_cells = 48;
# Used to compute P....
my $charge_max_u = 200;
my $charge_max_i = 16;
my $mode = 128;
my $unknown = 0;

sub str2int16 {
	my $s = shift;
	$value = hex($s);
	print "v:$value, hex:$s\n" if $debug > 1;
	$value -= 0x10000 if $value >= 0x8000;
	return $value;
}

sub str2uint16 {
	my $s = shift;
	$value = hex($s);
	return $value;
}

sub str2uint32 {
	my $s = shift;
	$value = hex($s);
	return $value;
}

sub str2float {
	my $s = shift;
	$value = unpack "f",reverse pack "H*", $s;
#	$value = unpack "f", pack "H*", $s;
	return $value;
}

sub DecodeCANmessage {
	my $id = shift;
	my $data = shift;

	my $i = 0;
        my @bytes;
	while ($i < length($data)) {
          $h = substr($data,$i*2,2);
	  push @bytes,$h;
          $i++;
        }
	
        my $p = 1;
	print "ID:$id,data:$data\n" if $debug > 0;
	my $statement = "";
	my $ret = "";

   $hit = 0;
   if ($id =~ /111100/) {
        $hit = 1;
	$ret = " Cell voltage limits\n";
	$mincellvoltage = str2uint16($bytes[1] .$bytes[0]) / 1000;
	$maxcellvoltage = str2uint16($bytes[3] .$bytes[2]) / 1000;
	#$avgcellvoltage = str2uint16($bytes[5] .$bytes[4]) / 1000;
	$num_mincellvoltage = hex($bytes[4]);
	$num_maxcellvoltage = hex($bytes[5]);
	$ret .= "  MinCell U: $mincellvoltage, MaxCell U: $maxcellvoltage, #min: $num_mincellvoltage, #max: $num_maxcellvoltage";
 	$statement = "UPDATE BMSDATA SET MAX_CELL_U = '$maxcellvoltage', MIN_CELL_U = '$mincellvoltage', NUM_MIN_CELL_U = '$num_mincellvoltage', NUM_MAX_CELL_U = '$num_maxcellvoltage', AVG_CELL_U = '$avgcellvoltage'";
   }
   if ($id =~ /111200/) {
        $hit = 1;
	$ret = " Cell temperature limits ($id)\n";
	$mincelltemp = hex($bytes[0]) - 40;
	$maxcelltemp = hex($bytes[1]) - 40;
	$num_mincelltemp = hex($bytes[2]);
	$num_maxcelltemp = hex($bytes[3]);
	$ret .= "  MinCell t: $mincelltemp, MaxCell t: $maxcelltemp, #min: $num_mincelltemp, #max: $num_maxcelltemp";
        $statement = "UPDATE BMSDATA SET MAX_CELL_T = '$maxcelltemp', MIN_CELL_T = '$mincelltemp', NUM_MIN_CELL_T = '$num_mincelltemp', NUM_MAX_CELL_T = '$num_maxcelltemp'";
#	print "$statement\n";
#exit(1);
   }
   if ($id =~ /111300/) {
        $hit = 1;
	$ret = " Cell bypass current summary\n";
	$num_bypass = hex($bytes[6]);
	$num_initialbypass = hex($bytes[4]);
	$num_finalbypass = hex($bytes[5]);
	$ret .= "  #initial_bypass: $num_initialbypass, #final_bypass: $num_finalbypass, #bypass: $numbypass";
        $statement = "UPDATE BMSDATA SET NUM_INITIAL_BYPASS = '$num_initialbypass', NUM_FINAL_BYPASS = '$num_finalbypass', NUM_BYPASS = '$num_bypass'";
   }

   if ($id =~ /111500/) {
        $hit = 1;
	$ret = " Shunt power monitoring ($id)\n";
	$soc = int((hex($bytes[0]) - 10) /2);
	$shunt_voltage = str2uint16($bytes[3] .$bytes[2]) / 100;
	$shunt_amperes = str2float($bytes[7] . $bytes[6] . $bytes[5] . $bytes[4]) / 1000;
	$shunt_power = $shunt_voltage * $shunt_amperes;
	$ret .= "  Shunt U: $shunt_voltage, Shunt I: $shunt_amperes A, Shunt P: $shunt_power W, SoC:$soc";
        $statement = "UPDATE BMSDATA SET SHUNT_U = '$shunt_voltage', SHUNT_I = '$shunt_amperes', SHUNT_P = '$shunt_power', SOC = '$soc'";
   }

   if ($id =~ /111800/) {
        $hit = 1;
	$remaining = str2float($bytes[7] . $bytes[6] . $bytes[5] . $bytes[4]) / 1000;
	$ret .= "  Remaining: $remaining Ah";
       $statement = "UPDATE BMSDATA SET CAPACITY_REMAINING = '$remaining'";
   }

   if ($id =~ /140300/) {
        $hit = 1;
	$ret = " Charge Control target limits ($id)\n";
	$ch_voltage = str2uint16($bytes[7] .$bytes[6]) / 10;
	$ch_amp = str2uint16($bytes[3] .$bytes[2]) / 10;
	$ret .= "  Charge U: $ch_voltage, Charge I: $ch_amp";
        $statement = "UPDATE BMSDATA SET TARGET_CHARGE_U = '$ch_voltage', TARGET_CHARGE_I = '$ch_amp'";
   }

   if ($id =~ /140400/) {
        $hit = 1;
	$ret = " Discharge target limits ($id)\n";
	$dh_voltage = str2uint16($bytes[7] .$bytes[6]) / 10;
	$dh_amp = str2uint16($bytes[3] .$bytes[2]) / 10;
	$ret .= "  Discharge U: $dh_voltage, Discarge I: $dh_amp";
        $statement = "UPDATE BMSDATA SET TARGET_DISCHARGE_U = '$dh_voltage', TARGET_DISCHARGE_I = '$dh_amp'";
   }
   if ($id =~ /1d11/) {
        $hit = 1;
	$ret = " Cellmon stats\n";
	$mincellvoltage = str2uint16($bytes[1] .$bytes[0]) / 1000;
	$maxcellvoltage = str2uint16($bytes[3] .$bytes[2]) / 1000;
        $cell_temp = hex($bytes[4]) - 40;
        $bypass_temp = hex($bytes[5]) - 40;
        $bypass_pwm = hex($bytes[6]);

	$ret .= "  MinCell U: $mincellvoltage, MaxCell U: $maxcellvoltage, CellTemp: $cell_temp, BypassTemp: $bypass_temp, BypassPWM: $bypass_pwm";
# 	$statement = "UPDATE BMSDATA SET MAX_CELL_U = '$maxcellvoltage', MIN_CELL_U = '$mincellvoltage', NUM_MIN_CELL_U = '$num_mincellvoltage', NUM_MAX_CELL_U = '$num_maxcellvoltage', AVG_CELL_U = '$avgcellvoltage'";

   }


   if ($id =~ /507/) {
        $hit = 1;
	$ret = " Control flag state ($id)\n";
        $crc_flags = hex($bytes[1]);
        $crc_flags_str = "";
        $crc_flags_str .= "Ok," if ($crc_flags & 1);
        $crc_flags_str .= "Transition," if ($crc_flags & 2)   ;
        $crc_flags_str .= "Precharge," if ($crc_flags & 4);
        $cha_flags = hex($bytes[2]);
        $cha_flags_str = "";
        $cha_flags_str .= "On," if ($cha_flags & 1);
        $cha_flags_str .= "Transition," if ($cha_flags & 2)   ;
        $cha_flags_str .= "Limited power," if ($cha_flags & 4);
        $dis_flags = hex($bytes[3]);
        $dis_flags_str = "";
        $dis_flags_str .= "On," if ($dis_flags & 1);
        $dis_flags_str .= "Transition," if ($dis_flags & 2)   ;
        $dis_flags_str .= "Limited power," if ($dis_flags & 4);

        $heat_flags = hex($bytes[4]);
        $heat_flags_str = "";
        $heat_flags_str .= "On," if ($heat_flags & 1);
        $heat_flags_str .= "Transition," if ($heat_flags & 2)   ;

        $cool_flags = hex($bytes[5]);
        $cool_flags_str = "";
        $cool_flags_str .= "On," if ($cool_flags & 1);
        $cool_flags_str .= "Transition," if ($cool_flags & 2)   ;

        $cell_flags = hex($bytes[5]);
        $cell_flags_str = "";
        $cell_flags_str .= "On," if ($cell_flags & 1);
        $cell_flags_str .= "Bypass temp relief," if ($cell_flags & 2)   ;

	$ret .= "  Critcical flags: $crc_flags_str, Charge flags: $cha_flags_str, Discharge flags: $dis_flags_str\n";
	$ret .= "  Heat flags: $heat_flags_str, Cool flags: $cool_flags_str, Cell balancing flags: $cb_flags_str";

   }


	if ( (length($statement)>0) and ($p) ) { # update db
		$statement .= " WHERE BMS_ID = 1";
#		print "executing: $statement\n";
		$dbh = DBI->connect($dsn_bms, $userid, $password, { RaiseError => 1 }) or die $DBI::errstr;
		$rv  = $dbh->do($statement, undef, $som_val, $id); 
		$DBI::err && die $DBI::errstr;
		my $timestamp = time();
		$statement = "UPDATE BMSDATA SET LAST_UPDATE = '$timestamp'";
		$rv  = $dbh->do($statement, undef, $som_val, $id); 
		$DBI::err && die $DBI::errstr;
		$dbh->disconnect();
	}

	$counter++;
	if ( ($counter == 10) and ($p) ) { # Check if it's time to update json and bmsdata
		$counter = 0;
                my $content_cache = get $cache_url;
                warn "Couldn't get $cache_url" unless defined $content_cache;
		my $e_produced = 0;
		my $e_consumed = 0;
		if ($content_cache =~/channels/) {
	                my $json_cache = decode_json($content_cache);
	                $channels = $json_cache->{'Body'}{'Data'}{'393216'}{'channels'};
		 	$e_produced = int($channels->{'BAT_ENERGYACTIVE_ACTIVEDISCHARGE_SUM_01_U64'} / 3600 );
		 	$e_consumed = int($channels->{'BAT_ENERGYACTIVE_ACTIVECHARGE_SUM_01_U64'} / 3600 );
			print "Produced: $e_produced kWh, Consumed: $e_consumed kWh\n";
		}
		open my $fh, '<', '/var/www/html/data/energy.json' or die "Can't open file $!";
		my $file_content = do { local $/; <$fh> };
		close $fh;
		my $e_p_yesterday = 0;
		my $e_c_yesterday = 0;
		my $date_lastupdate = "";
		my $dt   = DateTime->now;   # Stores current date and time as datetime object
		my $date_now = $dt->ymd;   # Retrieves date as a string in 'yyyy-mm-dd' format		my $date_now = strftime "%Y-%m-%d", localtime time;
		if ($file_content =~ /Produced/) {
	                my $json_file = decode_json($file_content);
			$e_p_yesterday = $json_file->{'Produced'}{'E_Tot_Yesterday'};
			$date_lastupdate = $json_file->{'LastUpdate'};
		}
		if ($file_content =~ /Consumed/) {
	                my $json_file = decode_json($file_content);
			$e_c_yesterday = $json_file->{'Consumed'}{'E_Tot_Yesterday'};
		}
#exit(1);

#                my $content = get $api_url;
#                warn "Couldn't get $api_url" unless defined $content;
#		if ($content =~/Data/) {
#	                my $json_out = decode_json($content);
#	                $e_now = $json_out->{'Body'}{'Data'}{'TOTAL_ENERGY'}{'Values'}{'1'};
#		}

		if (($e_produced > 0) and ($e_consumed > 0) ){ # update....
			my $e_p_today = $e_produced - $e_p_yesterday; 
			my $e_c_today = $e_consumed - $e_c_yesterday; 
			if ($date_now ne $date_lastupdate) {
				$e_p_yesterday = $e_produced;
				$e_c_yesterday = $e_consumed;
			} else {
			}	
                        open(FH, '>', '/var/www/html/data/energy.json') or warn $!;
                        print FH '{ ';
                        print FH ' "LastUpdate" : "' . $date_now . '",';
                        print FH ' "Produced" : { ';
                        print FH '   "E_Today" : ' . $e_p_today . ', ';
                        print FH '   "E_Now" : ' . $e_produced . ', ';
                        print FH '   "E_Tot_Yesterday" : ' . $e_p_yesterday;
                        print FH '  }, ';
                        print FH ' "Consumed" : { ';
                        print FH '   "E_Today" : ' . $e_c_today . ', ';
                        print FH '   "E_Now" : ' . $e_consumed . ', ';
                        print FH '   "E_Tot_Yesterday" : ' . $e_c_yesterday;
                        print FH '  } ';
                        print FH '} ';
                        close(FH);
		}


		$statement = "SELECT * FROM BMSDATA WHERE BMS_ID = 1";
		$dbh = DBI->connect($dsn_bms, $userid, $password, { RaiseError => 1 }) or die $DBI::errstr;
		$rv  = $dbh->prepare($statement);
		$rv->execute();
		my $ok2charge = 1;
		my $ok2discharge = 1;
		my @rows = $rv->fetchrow_array();
		if ($#rows > 24) {
			my $limit_cell_u_min = 3.6;
			my $limit_cell_u_max = 4.2;
			my $limit_cell_t_min = 1;
			my $limit_cell_t_max = 50;

			my $min_cell_u = $rows[1];
			my $max_cell_u = $rows[2];
			my $avg_cell_u = $rows[3];
			my $soc = $rows[16];
			my $min_cell_t = $rows[6];
			my $max_cell_t = $rows[7];
			my $shunt_u = $rows[13];
			my $shunt_i = $rows[14];

			open(FH, '>', '/var/www/html/data/bms.json') or warn $!;
			print FH '{ ';
			print FH ' "BMS" : { ';
			print FH '  "Data" : { ';
			print FH '   "Min_Cell_U" : ' . $min_cell_u . ', ';
			print FH '   "Max_Cell_U" : ' . $max_cell_u . ', ';
			print FH '   "Min_Cell_t" : ' . $min_cell_t . ', ';
			print FH '   "Max_Cell_t" : ' . $max_cell_t . ', ';
			print FH '   "Shunt_U" : ' . $shunt_u . ', ';
			print FH '   "Shunt_I" : ' . $shunt_i . ',';
			print FH '   "SoC" : ' . $soc;
			print FH '  } ';
			print FH ' } ';
			print FH '} ';
			close(FH);			
#		exit(1);
			# OK charge?
			if ($max_cell_u < $limit_cell_u_max) {
				$ok2charge = 1;
			}		
			# OK charge?
			if ($min_cell_u > $limit_cell_u_min) {
				$ok2discharge = 1;
			}
			# Temp too high?		
			if ($max_cell_t > $limit_cell_t_max) {
				$ok2charge = 0;
				$ok2discharge = 0;
			}
			# Temp too low?		
			#if ($min_cell_t < $limit_cell_t_min) {
			#	$ok2charge = 0;
			#	$ok2discharge = 0;
			#}
		} else {
			print "no data!\n";
			exit(1);
		}
		$DBI::err && die $DBI::errstr;
		$rv->finish();
		$statement = "UPDATE BMSDATA SET OK2CHARGE = '$ok2charge', OK2DISCHARGE = '$ok2discharge'";
		$statement .= " WHERE BMS_ID = 1";
#		print "U:$statement\n";
#		exit(1);
		$dbh = DBI->connect($dsn_bms, $userid, $password, { RaiseError => 1 }) or die $DBI::errstr;
		$rv  = $dbh->do($statement, undef, $som_val, $id); 
		$DBI::err && die $DBI::errstr;
		$dbh->disconnect();
	}
	return $ret;
}


my $timestamp = time();
print "starting to decode can messages at $timestamp\n";
$debug = 0;

$dbh = DBI->connect($dsn_bms, $userid, $password, { RaiseError => 1 }) or die $DBI::errstr;
$statement = "UPDATE BMSDATA SET TARGET_CELL_MAX_U = '$target_cell_max_u', TARGET_CELL_MIN_U = '$target_cell_min_u'";
$rv  = $dbh->do($statement, undef, $som_val, $id); 
$DBI::err && die $DBI::errstr;
$statement = "UPDATE BMSDATA SET TARGET_SOC_MAX = '$target_soc_max', TARGET_SOC_MIN = '$target_soc_min'";
$rv  = $dbh->do($statement, undef, $som_val, $id); 
$DBI::err && die $DBI::errstr;
$statement = "UPDATE BMSDATA SET NUM_CELLS = '$num_cells'";
$rv  = $dbh->do($statement, undef, $som_val, $id); 
$DBI::err && die $DBI::errstr;
$statement = "UPDATE BMSDATA SET TARGET_CHARGE_U = '$charge_max_u', TARGET_CHARGE_I = '$charge_max_i'";
$rv  = $dbh->do($statement, undef, $som_val, $id); 
$DBI::err && die $DBI::errstr;
$statement = "UPDATE BMSDATA SET TARGET_DISCHARGE_U = '$charge_max_u', TARGET_DISCHARGE_I = '$charge_max_i'";
$rv  = $dbh->do($statement, undef, $som_val, $id); 
$DBI::err && die $DBI::errstr;
$statement = "UPDATE BMSDATA SET MODE = '$mode', UNKNOWN = '$unknown'";
$rv  = $dbh->do($statement, undef, $som_val, $id); 
$DBI::err && die $DBI::errstr;
$dbh->disconnect();


my %can_registers;
my $j = 0;

while (1) {

  my $timestamp_now = time();
  print "\n\nTimestamp:$timestamp_now\n";
  $dbh = DBI->connect($dsn_can, $userid, $password, { RaiseError => 1,sqlite_open_flags => DBD::SQLite::OPEN_READONLY }) or die $DBI::errstr;
  $statement = "SELECT * FROM BATRIUM WHERE ID LIKE '%1_____'";
  $sth = $dbh->prepare($statement) or die "error";
  $sth->execute() or die "error";
  my($id,$lastupdate,$data);
  while(($id,$lastupdate,$data) = $sth->fetchrow()) {
    my $timestamp_now = time();
    print "R:$id, D:$data, L:$lastupdate($timestamp_now)\n";
    $lastupdate = int($lastupdate);
    if ( ($lastupdate + 60 < $timestamp_now) and ($lastupdate > 0) ) {
        print "Timeout on CAN ($lastupdate + 60 < $timestamp_now\n";
	exit(1);
    }
    $decoded = DecodeCANmessage( $id, $data );
#    if ($id =~ /501/) {
      print "Reg:$id, Data: $data - $decoded                             \n";
#    }
  }
  $sth->finish();
  $dbh->disconnect();
  sleep 10;
}

