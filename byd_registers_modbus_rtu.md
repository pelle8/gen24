## BYD Modbus Registers

First of all, thanks to "Flo" and "Dala" for the great coop "reversing" the registers involved. And, thanks to Alex for letting me listen to live data between his BYDs and Gen24.

```

Polled at startup:
101 - "SI",1
103 - "BYD",0,0,0,0,0,0,0,0,0,0,0,0,0,0                     type
119 - "BYD Battery-Box Premium HV", 0, 0, 0                 descr
135 - "5.0", 0, 0, 0, 0, 0, 0, "3.16", 0, 0, 0, 0, 0, 0     version
151 - "Pelle", 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0        serial
167 - 1, 0 

101,103,119 matters



Polled at startup:
201 - 0         always 0
202 - 0         always 0 
203 - 44236     Capacity (44kWh)
204 - 40960     Max Power (41kW)
205 - 40960   	Max Power (41kW), lowest value of 204 and 205 will be enforced by Gen24 
206 - 4672      Max Voltage (467.2V), if higher charging is not possible (goes into forced discharge)
207 - 3200      Min Voltage (320.0V), if lower Gen24 disables battery
208 - 53248     always 53248 for this BYD, Peak power (53kW)
209 - 10        always 10
210 - 53248     always 53248 for this BYD, Peak power (53kW)
211 - 10        always 10
212 - 0         always 0
213 - 0         always 0

204-207 matters, rest seems informational
208-211 doesn't change

Polled every 5s:
301 - 3         status(*): ACTIVE - [0..5]<>[STANDBY,INACTIVE,DARKSTART,ACTIVE,FAULT,UPDATING]
302 - 0         always 0
303 - 128       mode(*): normal
304 - 3200      soc: 32%
305 - 24000     tot cap: 24kWh
306 - 7680      remaining cap: 7.68kWh
307 - 0         max/target discharge power: 0W (0W > restricts to no discharge)
308 - 4312      max/target charge power: 4.3kW (during charge), both 307&308 can be set (>0) at the same time
309 - 1734      Batt Voltage outer (0 if status !=3, maybe a contactor closes when active): 173.4V
310 - 61760     Current Power to API: if>32768... -(65535-61760)=3775W
311 - 1732      Batt Voltage inner: 173.2V
312 - 61760     =r310
313 - 140       temp min: 14 degrees (if below 0....65535-t)
314 - 150       temp max: 15 degrees (if below 0....65535-t)
315 - 0         always 0
316 - 0         always 0
317 - 101       counter charge hi
318 - 9912      counter charge lo....65536*101+9912 = 6629048 Wh?
319 - 0         always 0
320 - 0         always 0
321 - 92        counter discharge hi
322 - 7448      counter discharge lo....65536*92+7448 = 6036760 Wh?
323 - 230       device temp (23 degrees)
324 - 9850      soh (value/100 %) = 98.5%

301,303-308 matters

* r301: Follows r401. During startup, first response is 1, next response is 3 (after write r400=2) 
* r303: b0: charging, b1: discharge (status-bits back to Gen24)
        b7: normal, b6: energy-saving, b5: calibrate, b4: forced loading
        - Calibrate om my fake BYD, ended up with an error message in the GUI...check DC connection
        - Normal operation: [128..130] <> [idle,charging,discharging]
        - Force load: [144,145] <> [idle,charging 500W]



Polled every 60s:
401 0002 0000 => r301=0, 0001 => r301=1, 0002 => r301=3, Gen24 writes 
402 ff00 toggles 00ff-ff00 (Gen24 reads value from 402, and then toggles it and writes to BYD)
403 003c always 003c
404 0000 always 0
405 0000 always 0
406 63bf = unix epoch timestamp hi ...timestamp for last error/restart?
407 9b67 = unix epoch timestamp lo ...63bf 9b67 => Thursday 12 January 2023 09:59:24
408 0000 always 0
409 0001 always 0001 when 406 and 407 has been written 
410 0202 0202 when 301=3, 0002 when 301=0
411 0101 0101,0102,0103 seen
412 0000 always 0
413 0000 always 0
414 0000 always 0
415 0000 always 0
416 0000 always 0
417 0000 always 0
418 0fb2 =r311 Gen24 Measured Voltage? (4018 => 401,8V)
419 0fb7 =r309 BYD reported Voltage (4023 => 402,3V)

401-409 Gen24 writes on startup
402 is the only register that Gen24 reads!


Polled every 20s
1001-1100       zeroes normally....but was filled with timestamp etc during one exception
1001-1100       temporary error code? - doesn't seem to matter, just let Gen24 read...


???
12289-13057     unknown (gen24 writes) - doesn't seem to matter



Startup (This takes about 10sec with real BYD):
Read 100,102,118,166,100
Read 200
Read 300 (1,0,128...)
Write 400 (1,65280...)
Read 401
Write 401 (toggle)
Read 1000 (0,0,0...)
Read 300 (1,0,128...)
Write 400 (2)
Read 300 (3,0,128...)

```
