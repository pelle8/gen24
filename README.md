# Inverter resources

This repo contains a bit of everything BYD register, scripts, logs, wiring etc.
If you are looking for the RPi code to hook up a battery to a Gen24, head over to: https://gitlab.com/pelle8/batteryemulator

## Caution - Use At Your Own Risk
HV batteries can be dangerous, and applying wrong configuration or running code in an unintended way can cause danger to yourself or the battery. 
Use the code, modify it, but don't blame me :-)

## Registers
- [BYD modbus](./byd_registers_modbus_rtu.md)
- [BYD CAN](./byd_registers_can.md)

## Wiring a Tesla M3 battery
[Wiring](./supporting/tesla_m3_wiring/README.md)...

## Wiring a Juntek shunt
[Wiring](./supporting/juntek_wiring/README.md)...

## Scripts
Requires - pymodbus (I'm runnning 2.5.3 on python 3.7)
Most of the scripts below are modified ones from pymodbus pages.

### rs485_modbus_listener.py
Listens passively on RS485 and logs modbus-messages. A bit rough at the moment.

### rs485_meter_gen24_control.py
Getting info from a grid meter over RS485 and control Gen24 charge/discharge with modbus tcp.

### byd_interrogator.py
This script hooks into an existing RS485 bus, between a Gen24 and a BYD, and sends queries to the BYD. The modbus-responses are saved into a logfile. Data will also be collected from the Gen24 API-pages (over IP), the responses are saved in subdirs (named as a timestamp for easier correlation with modbus-log).
The collection runs at a regular interval (default 60s).

### server.py
This one emulates a BYD battery with a static SoC. When run, a battery should pop as connected in the Gen24 webgui.
You need a RS485 interface connected to Gen24.


### server_db.py
Same as server_db.py, but takes values from a database which makes it more applicable in real life....where SoC changes.
If you have a BMS, values from that one should be injected in the database that this script reads from.
_You will need to adjust this to your own setup, this only serves as a base or food for thought!_

### create_db.py
Creates the db that is used by server_db.py

### client_modbus.py
This one connects over modbus rtu to a Gen24.

### client_tcp.py
This one connects over modbus tcp to a Gen24 and controls charging/discharging using SunSpec.
Only IP connectivity and configuration in Gen24 are required to enable communication over modbus/tcp.

First tests shows that you can control charging and discharging, together with a setting for power. The latter setting is a bit weird since its a percentage of the capacity of the battery (from 200-registers during RS485 battery setup). Nevertheless the wanted 
power can be computed.

## Logging

### RS485 logging

Preferred is to use a passive listener, since the Gen24 reads a lot collisions will be hard to avoid. See script above.

### CAN logging

- install can-utils: `sudo apt install can-utils`
- configure bitrate: `sudo ip link set can0 type can bitrate 500000`
- make sure can-interface is up: `sudo ip link set can0 up`

Use candump - `candump -L can0 > /tmp/can.log`

To replay - `canplayer -I /tmp/can.log`


sudo modprobe vcan
sudo ip link add dev vcan0 type vcan
sudo ip link set up vcan0

## Todo Gen24

- investigate how calibrate-mode works, maybe part of startup with a real BYD
- r4xx contains data that the Gen24 never seem to read...

