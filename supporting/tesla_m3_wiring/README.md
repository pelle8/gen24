# Wiring a Tesla M3 battery

When I got my battery, both the pyro fuse and one glass fuse was blown. While waiting for spare parts to arrive, I used cables instead. 
The Tesla BMS has a bunch of safety mechanisms in place (HVILs). If you have the cables and connectors you are good since these will take care of most of the HVILs, if not...you have to close the HVILs yourself - using jumpers/cables. Like I did.

Below is the HVIL for the AC connector, jumper location is shown (not the actual jumper itself).

![HVIL AC](./hvil_ac.jpg)

With the penthouse lid open, there are jumpers for the two HV-connectors. Also, the pyro fuse has been removed and replaced with a cable and a jumper (I tried a 11ohm resistor for the jumper first, but that didn't work).

![HVIL HV connectors](./hvils_and_pyro_fuse.jpg)

Next, the X098 connector had a few conections itself, scroll down to see where the pins should go...
Since I wanted to run with the penthouse lid open, I had to connect ground to the two holes on each side of X098.

![X098](./X098.png)

The connector above will need the following connections...(apart the grounding of the holes if the lid is open)
```
Pin 1 and 3 - I have a 68 ohm resistor here (should possibly be 60 ohm, but 68 works)
Pin 8 amd 18 - To +12V
Pin 9 - GND
Pin 16 and 15 - To CAN-interface on RPi 
```

Make sure to also hook up 12V to the two screws close to the X098....
![Battery](./battery_conn.jpg)

And finally, hook up the inverter and connect a capacitor (I use a 1000uF/600V) to one of the HV connectors...

![capacitor](./capacitor.jpg)

Note the yellow/green cable, that is a temporary replacement for a blown fuse.

