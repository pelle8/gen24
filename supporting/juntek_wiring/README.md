# Wiring a Juntek shunt

## HV
- Connect HV- from the battery to one side of the shunt
- Connect HV- from the other side of the shunt to the inverter
- Connect HV+ from the battery to the inverter
- Connect HV+ to VIN on Juntek

## LV
- Connect GND on Juntek to power source, GND
- Connect VEXT on Juntek to power source, +12V

## RS485
- Keep the USB-cable, that is intended for display-shunt usage. Remove insulation from DATA+ and DATA- on USB-cable.
- Connect A+ on RS485-dongle/interface to DATA- on spliced cable (normally white)
- Connect B- on RS485-dongle/interface to DATA+ on spliced cable (normally green)

## Juntek Display
The RPi can either listen passively or ask the shunt itself.

- When in passive mode, the display needs to be attached to the shunt since the display is doing the interrogation.
- In master mode, the display cannot be attached with USB.

